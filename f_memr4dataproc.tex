Device variability (sec.~\ref{sec:memstorage}) and volatility (sec.~\ref{sec:synapplast}) were mentioned as current challenges for most applications based on memristive memories.
This is in contrast with biological systems, which are not built in clean rooms, and it is hard to believe that evolution exploits ideal systems in reference to its own architecture design.
Biological systems perform despite noise, nonlinearity, variability, lack of robustness, and volatility.
Whether these ingredients hinder the performance of biological systems or are actually a building block for it, it is still unknown.
Sometimes they are avoided, not because it would have an undesired effect in practice, but simply because its effect cannot be easily modeled or studied (e.g. we have but a few tools to deal with nonlinear systems intrinsically, beyond the iteration of linear methods).
Hence, there is no reasons to believe that eliminating naturally occurring properties is the path to success in achieving artificial systems that perform comparably to biological ones.

This section overviews some applications that embrace device volatility~\citep{Carbajal2015}, nonlinear transients, and variability~\citep[][sec. 7.5]{Eliasmith2002}, to implement learning methods with memristors.
To put these methods within an unifying framework, we first review the concept of analog computation, and discuss its relation to the physical substrate on which it is implemented.

\subsection{Analog computation}
\label{sec:analogc}
In order to pin down the concept of analog computation we compare analog and digital computers, assuming that the latter is familiar to most readers.
The principal distinction between analog and digital computers is that digital operates on discrete representations in discrete steps, while analog operates on continuous representations, i.e. discrete vs. continuous computation~\citep[refer to][for a complete discussion and historical overview]{MacLennan2012}~\citep{MacLennan2014, MacLennan2017}.

In all kinds of computation the abstract mathematical structure of the problem and the algorithm are instantiated in the states and processes of the physical system used as computer~\citep{Horsman2014}.
For example, in the current digital computer, computation is carried out using strings of symbols that bare no physical relationship to the quantities of interest; while in analog computers the latter are proportional to the physical quantities used in the computation.
Figure~\ref{fig:encode_decode_AC} illustrates the relation between representation of the problem in the designers mind and instantiation in the computers states.

\begin{figure}[htpb]
  \centering
  \begin{tikzpicture}[auto,node distance=3.5cm,->,>=stealth,semithick]
    \tikzset{mycloud/.style={cloud, cloud puffs=10.73, cloud ignores aspect, minimum width=2cm, minimum height=1cm, align=center, draw, fill=GreenYellow!40}}
    \tikzset{mystate/.style={rectangle, minimum width=2cm, minimum height=1cm, align=center, draw=BrickRed}}

    \node[mystate] (p)                 {$U$};
    \node[mycloud] (mp)  [above of=p]  {$V$};
    \node[mycloud] (mtp) [right of=mp, xshift=2cm] {$W$};
    \node[mystate] (pt)  [below of=mtp, xshift=0.5cm]  {$X$};
    \node[mycloud] (mpt) [above of=pt, yshift=-0.6cm]{$W^\prime$};

    \path[shorten >=2mm, shorten <=2mm, ultra thick]
          (pt)  edge          node [left, text width=2cm, align=right] {decode or represent} node [right] {$\mathcal{R}$} (mpt)
          (mp)  edge [dashed] node {COMPUTATION} (mtp)
          (mp)  edge          node [left, text width=2cm, align=right, yshift=-0.15cm] {encode or instantiate} node [right, yshift=-0.15cm] {$\mathcal{E}$} (p)
          (p)   edge          node [above] {real world} node [below] {$\mathcal{D}(\bm{x},\bm{u})$} (pt);

    \draw[line width=1pt, blue, -, double, opacity=0.5] (-1.2,1.05) -- (7.2,1.05);
  \end{tikzpicture}
  \caption{
    Computing with dynamical systems adapted from~\citep{Horsman2014}.
    Conceptual depiction of the sets and transformations involved in a typical computation using a dynamical system $\mathcal{D}$.
    In this case, the computation is defined by its action on the input-output set $V$, $W$.
    The inputs $\bm{u}(t)\in U$ to $\mathcal{D}$ are encoded or instantiated versions of $V$ through the transformation $\mathcal{E}$.
    The output of the dynamical system $\bm{x}(t) \in X$ is decoded or represented back into the set $W^\prime$ via the readout transformation $\mathcal{R}$.
    When $W$ and $W^\prime$ are similar, the composed transformation $\mathcal{R}\circ \mathcal{D}\circ \mathcal{E}$ is a proxy for the sought computation.
    }
    \label{fig:encode_decode_AC}
\end{figure}

We can decompose computation in three stages: i) encoding, ii) processing, iii) decoding.
Programming a computer implies first encoding our input to the processing unit and then decoding the output to obtain the result of the computation.
These three stages require an advanced understanding of the behavior of the physical substrate, how it reacts to inputs and how it transforms its states.
This is true if the computer is meant to implement an universal model of computation, or if it is specialized hardware optimized to solve a particular subclass of problems.

The encoding and decoding maps relate the states of the computer to our understanding of the problem, and their definition is a recurring challenge in the design of computers.
We mentioned this difficulty when building memristive synaptic arrays with plasticity (sec.~\ref{sec:synapplast}); data representation in biological systems is still an open research field, and natural systems tend to smear out our pristine categorizations of encodings.
There is yet another difficulty to attend to when defining encoding and decoding maps.
To avoid confounding, the class of maps needs to be restricted, because ill-defined maps (e.g. encryption) will complicate computation, while too sophisticated maps could render the contribution of the computing device negligible.
The former will deteriorate the computing performance, while the latter is just bad design.
In other words, the problem needs to be specified using the "language" of the computing device.
Using the wrong language increases the difficulty of the problem, and consequently decreases performance.
To understand the language of the device we need the equivalent of Shannon's analysis of the differential analyzer~\citep{Shannon1941}.
The encoding-decoding pair is also linked to the "natural basis of computation"~\citep{Borghetti2010} of a device, which refers to the description of the device behavior suited for the computation purposes.

The success of digital computers is in part given by the efficiency to instantiate and process a universal model of computation able to solve all kinds of computation problems, as conjectured by the Church-Turing(-Deutch) thesis~\citep{Deutsch}.
This is achieved by a precise control of each step of the computation and the way the computer transforms (or evolves) its states.
Another advantage of modern digital computers over analog prototypes is the very high precision they provide for the instantiation and solution of a problem's quantities.
This high precision, however, is unnecessary in many engineering applications, in which the input data are known to only a few digits, the equations may be approximate or derived from experiments, and the results are not sensitive to round-off errors~\citep[see][for an overview]{Moreau2018}.
Therefore, research into specialized hardware (analog of digital) is a worthy activity.

Since analog computers escape the frame of relevance of the Church-Turing thesis, it has been argued that they can be more powerful than digital computers\citep[][sec. "Analog Computation and the Turing Limit"]{MacLennan2012}.
Besides this benefit, it is worth exploring the efficiency of analog computers to solve subclasses of problems, i.e. specialized analog hardware, and to understand their pervasiveness in natural systems, perhaps linked to the precision attained by systems built from many imprecise cheap modules.
This is not a simple task, since many of these analog computers outsource some of the process control present in digital computers to the natural dynamics of the physical substrate, elevating the bar on the level of understanding required to build and program them.
It is possible to achieve a significant speedup in computational time, usually at the expense of other quantities; for instance~\citep{Traversa2015a} reported an speedup for NP-Complete problems at an exponential cost in energy.

As an example of an analog computer let us consider an hydrostatic polynomial root finder proposed by ~\citeauthor{LEVISiam}~\citep{LEVISiam} .
Consider the following polynomial equation:

\begin{equation}
    \sum_{i=1}^n x^i c_i  + c_0= 0,
\end{equation}

\noindent The aim is to find a real value $x$ for which the equality holds, i.e. we want one root of the polynomial $p(x) = \sum_{i=1}^n x^i c_i + c_0$.
For the sake of this illustration, lets consider the case $n=2$.
In Fig.~\ref{fig:rootsol} we show the design of an hydrostatic root solver.
In one side of a pivoting lever, a certain shape is set to represent each term in the derivative of the polynomial (e.g. flat for the derivative of $c_1 x$, linear for the derivative of $c_2 x^2$).
On the other side of the lever we set a weight for the constant term.
When the device is submerged into the water holding the pivoting point, the depth at which the device equilibrates (i.e. becomes horizontal) is a solution of the polynomial equation.

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.6\textwidth]{RootSol.png}
    \caption{
    The hydrostatic root solver.
    Shapes encode the polynomial coefficients.
    A weight encodes the constant term.
    The depth at which torque is zero, is a solution of the polynomial equation.
    }
    \label{fig:rootsol}
\end{figure}

The example above illustrates the major role played by the understanding of the physical device, and how it allows us to encode and solve the specific problem we are interested in.
As mentioned before this is common to all sort of computers; we proceed mentioning a few.
The electronic digital computer exploits the behavior of transistors to encode the symbols of the computational model (essentially Boolean logic) it uses for computation.
Quantum annealers, considered for efficiently solving Mixed Integer quadratic Programming~\citep[][sec. 2.7]{axehill2008}, use the spins of a physical system and the computation of a quantum Ising model to solve the optimization~\citep{Venegas-Andraca2018}.
DNA computing exploits the self-assembly and preferential attachment of DNA strands to encode tiling systems with Wang tiles~\citep{Rothemund2004, Qian2011}.
Slime molds~\citep{Gale2015a} computation exploits the behavior of these protists to implement distributed optimization, e.g. shortest path, using the position of nutrients on the Petri dish to encode problems, and chemotatic behavior of the mold to solve them; the solution is read out from the distribution of mold cells in the Petri dish.
Ant colony optimization~\citep{Dorigo1997} is inspired by the behavior of their natural counterpart and uses digital models of pheromone dynamics for computation (see also stigmergy~\citep{Bonabeau1999}).

In the subsequent sections we describe in some technical details the algorithms meant to realize computers using memristive systems.

\subsection{Generalized linear regression, Extreme Learning Machines, and Reservoir computing}
\label{sec:reservoir}

Arguably the most used method to relate a set of values $X$ (inputs) to another set of values $\bm{y}$ (outputs) via an algebraic relation is linear regression: $\bm{y} = X^\top \bm{\beta}$.
However, in many realistic situations we believe that the relation between these sets of values is unlikely to be linear.
Hence a nonlinear counterpart is needed.
The simplest way to generalized a scalar linear regression model between inputs and outputs is to apply a nonlinear transformation to the inputs to obtain a new linear model $\bm{y} = G(X)^\top \bm{\gamma}$, where only the coefficient vector $\bm{\gamma}$ (or matrix when the output is not scalar) is learned from the data.
This nonlinear method is a linear regression in a space generated by nonlinear transformations of the input (see kernel methods~\citep{Muller2001, Hofmann2008}).
In neuromorphic computation the transformation is commonly given a particular structure by choosing a set of $N_g$ nonlinear functions and applying it to each input vector:

\begin{equation}
G(X)^\top_{ij} = g_j(\bm{x}(i)), \quad i=1,\ldots,N\; j=1,\ldots,N_g.
\end{equation}

This method is known as Extreme Learning Machines (ELM)~\citep{Huang2004ELM} and has been implemented in hardware exploiting the variability of fabricated devices to generate the nonlinear transformations~\citep{Patil2017, Parmar2017}.
Briefly explained, the differences among the devices generate different outputs for the same input, which are then used as the dictionary $G(X)^\top_{ij}$.

This formulation resembles two other mathematical methods that are worth mentioning: generalized Fourier series and generalized linear methods.
The relation with generalized Fourier series is made evident when the samples have a natural ordering (e.g. not i.i.d. time samples $i \leftrightarrow t_i$), then we can write the regression model as

\begin{equation}
y(t) = \sum_{k=1}^{N_g} \gamma_k g_k(\bm{x}(t)),
\end{equation}

\noindent which has the structure of a truncated generalized Fourier series (but not all the ingredients).

The resemblance with generalized linear models is made evident by considering the function $g(\bm{x}) = \ell^{-1}(\bm{x}^\top\bm{\eta})$ ($\ell$ is the link function) and $N_g = 1$; we obtain:

\begin{equation}
y_i = \gamma_1 \ell^{-1}(\bm{x}(i)^\top\bm{\eta}).
\end{equation}

\noindent However generalized linear models require that we learn the vector of coefficients $\bm{\eta}$ from the data, rendering the problem nonlinear.
This breaks the analogy with ELM in which the $\bm{\eta}$ vector should be fixed a priori.
The analogy is somehow rescued if we are given a distribution $p(\bm{\eta})$ for the $\bm{\eta}$ vector encoding prior knowledge or beliefs about the solution to the generalized linear model.
In this case we can build an ELM with $N_g \gg 1$

\begin{equation}
y_i = \sum_{k=1}^{N_g}\gamma_k \ell^{-1}(\bm{x}(i)^\top\bm{\eta}_k) \approx \int_{H} \ell^{-1}(\bm{x}(i)^\top\bm{\eta}) p(\bm{\eta})\ud\bm{\eta},
\end{equation}

\noindent where the set of $\bset{\bm{\eta}_k}$ coefficient vectors are sampled from the prior distribution, expecting that model averaging~\citep{Hoeting1999} will approximate the generalized linear model.

Summarizing, ELM uses a linear combination of a predefined dictionary of functions to approximate input-output relations.
The next step of generalization is Reservoir Computing (RC)~\citep{Jaeger01,Maass2002,Verstraeten09}, in which the $N_g$ functions are the solution of a differential or difference equations using the data as inputs, e.g.

\begin{align}
\bm{u}(t) &= \mathcal{E}(\bm{x})(t) \; \in \mathbb{R}^{\dim\bm{u}},\\
\mathcal{D}_{\bm{\lambda}} \bm{q} &= B \bm{u}(t), \; B \in \mathbb{R}^{\dim\bm{q}\, \times\, \dim\bm{u}},\\
%\bm{g} & = \bset{q_i}_{i \in I}\,, \; I \subseteq \bset{1, \ldots, \dim{\bm{q}}}, \; \vert I \vert = N_g \leq \dim{\bm{q}}\\
%\bm{g} & = H\bm{q}\,, \; H \in \bset{0,1}^{N_g \times\, \dim\bm{q}}\,, \; \forall i,j\, \sum H_{i:} = \sum H_{:j} = 1\\
\bm{g} & = H\bm{q}\,, \; H \in \mathbb{R}^{N_g \times\, \dim\bm{q}},\\
y(t) &= \bm{\gamma}^\top \bm{g} = \sum_{k=1}^{N_g} \gamma_k g_k(t, \bm{u}(t), \bm{\lambda}) \label{eq:RCreadout}
\end{align}

\noindent where the differential or difference equations are denoted with $\mathcal{D}_{\bm{\lambda}}$ (operator notation) with $\bm{\lambda}$ a vector of parameters that includes physical properties and the boundary (initial) conditions, and $\dim{\bm{q}} \geq N_g$.
The connectivity matrices $B$ and $H$ are typically random, the latter mixes the $\dim\bm{q}$ states to obtain $N_g$ signals (these could also be nonlinear mappings).
As explained in section~\ref{sec:analogc}, the input data is encoded by the transformation $\mathcal{E}$ (or $B \circ \mathcal{E}$) to properly drive the system.
This encoding, the operator $\mathcal{D}_{\bm{\lambda}}$, and the connectivity matrices are defined a priori, and only the coefficients $\bm{\gamma}$ combining the $\bm{g}$ functions are learned from the data, as in ELM.
These coefficient (or $\bm{\gamma}^\top H$) define the readout transformation $\mathcal{R}$ (see Fig.~\ref{fig:encode_decode_AC}).

The generalization proposed by RC is made obvious with the choice of arguments for the component functions $\bset{g_k}$ in eq.~\eqref{eq:RCreadout}: they can have i) an intrinsic dependence on time, e.g. autonomous behavior of the dynamical system; ii) they depend on the inputs, and iii) they depend on the properties of the dynamical system.
Property ii) says that these functions are not fixed as in ELM, they are shaped by the data signal.
Stated like this, the problem of implementing computation with reservoirs is strongly related to a nonlinear control problem.

RC allows for machine learning applications using natural or random dynamical systems, as opposed to carefully engineered ones.
Its capacity to exploit wildly different physical substrates for computation has been recently highlighted~\citep{Dale2018}.
The only strong requirement is that we are able to stimulate states of the system independently with signals encoding the input data, a classical example is the perceptron in a water bucket~\citep{Fernando2003}.
Hence, RC implementations using memristive networks has received considerable attention (software~\citep[see][and references therein]{Carbajal2015} and hardware~\citep{Du2017}).
In these applications, the memristive system (single device or network) plays the role of the dynamical system $\mathcal{D}_{\bm{\lambda}}$ (cf.~\ref{fig:encode_decode_AC}), the decoding map is a linear readout of several system states (memories, currents, voltage drops, etc.), and the encoding is usually tailored for the given application at hand.

The case of memristor based RC using the HP-memristor, eqns.~\eqref{eq:memristivedyn}-\eqref{eq:memristivestate}, is fairly well understood: a differential equation to simulate the propagation of signals across the circuit and the interaction between memristors has been derived in~\citep{Caravelli2017} (see also eqn.~\eqref{eq:memnet}).
In those equations the role of the circuit topology is extremely important in the collective dynamics of the circuit and in processing the input information.
When working with RC and memristors, it is important to prevent the saturation of all devices, since a saturated memristor becomes a linear resistor that only scales the input.
That is, RC heavily relies in the nonlinear (and volatile) behavior of the dynamical system.

\subsection{Neural engineering framework}
\label{sec:NEF}
The Neural engineering framework~\citep{Eliasmith2002} exploits our current understanding of neural data processing to implement desired computations.
It confines linear models to a particular class of basis functions, inspired by biologically plausible neuron models but not restricted to them.
Here we describe this framework in the case of function representation, the structure of the framework is analogous to other representation instances (scalar, vector, etc.).
The reader is referred to the original work~\citep{Eliasmith2002} for a comprehensive description.

The framework entails the characterization of an admissible set of functions that can be represented by a population of $N$ neurons.
In particular, the functions and their domain need to be bounded: $f: (x_\text{min}, x_\text{max}) \rightarrow (f_\text{min}, f_\text{max})$.
These functions are then encoded by a population of neurons with a predefined set of encoders of the form:

\begin{align}
a_i(f(x)) &= G_i\left(I_i(f(x))\right) \label{eq:nefencode},\\
I_i(f(x)) &= \alpha_i \braket{f(x)\tilde{\phi}_i(x)} + I_i^{\text{bias}}, \label{eq:somacurrent}
\end{align}

\noindent where $I_i$ represents the total input current to the $i$-th neuron soma.
The functions $a_i$ and $G_i$ are the tuning curves observed by neurophysiologists and the response function of the $i$-th neuron, respectively.
$G_i$ is a biologically inspired model of the firing rate, e.g. integrate-and-fire (LIF) neuron.
The encoding generators $\bset{\tilde{\phi}_i}$ (analogous to preferred directions) are defined a priori, and $\braket{f(x)\tilde{\phi}_i(x)}$ is a functional defined on the space of functions to be represented, e.g. in the original description this is the mean over $x$.
These encoders convert the function $f(x)$ into firing rates (or actual spike counts for the case of temporal encoding).

The encoding is matched with a corresponding decoding procedure that brings firing rates (spike counts) back to the function space.
The decoder takes the generic form:

\begin{equation}
\hat{f}(x) = \sum_{i}^N a_i(f(x)) \phi_i(x) \label{eq:nefdecode},
\end{equation}

\noindent where $\phi(x)$ are the unknowns of the framework.
That is, given some input $x$ and neural population's firing rates (spike counts) we can build functions of the input using the decoders $\bset{\phi_i}$.
In the original formulation the decoders are obtained via minimization of least square errors (with regularization in the case of noisy encodings), but other methods could be used, e.g. optimal $L_2$ dictionaries~\citep{Sheriff2017}.

The framework defined in eqns.~\eqref{eq:nefencode}-\eqref{eq:nefdecode} can realize arithmetic on functions of the input as well as nonlinear transformations.
It has also been used to represent linear time-independent systems with neuromorphic hardware~\citep{Corradi2014}.

NEF, RC, and ELM use the same form of decoding: the output is the scalar product of a input-independent vector with a input-dependent one.
The input-dependent vector is given by intrinsic properties of the computing device, it is the results of internal mechanisms.
The decoders are learned from data, and different decoders implement different computations (on the same input-dependent vectors).
However, RC and ELM learn a finite dimensional vector $\bm{\gamma} \in \mathbb{R}^{N_g}$ while NEF, in the functional form shown here, needs to learn $N$ infinite dimensional decoders.
The latter is mildly relaxed if the input domain is discretized with $N_x$ points, rendering the functional decoders $N_x$-dimensional vectors.
In general it is expected that $N_g \ll N_x N$, that is the degrees of freedom of NEF, is higher that the one of RC and ELM.
Hence NEF has the risk to transfer all the computation to the decoders making the contribution of the neural population marginal (or even an obstacle).
Extra regularity assumption on the decoders $\bset{\phi_i(x)}$ (eq.~\eqref{eq:nefdecode}) are needed to match decoders effective capacity to the capacity of the dynamic responses $\bset{g_i(x)}$ (eq.~\eqref{eq:RCreadout}).

Memristor networks can be used to implement NEF, by implementing the response functions $G_i$ of neural models. The $G_i$ functions corresponding to LIF neuron model is

\begin{equation}
G[I(z)] = \begin{cases}\frac{1}{\tau_0 - \tau_{\text{RC}}\log\left(1 - \frac{I_F}{I(z)}\right)} & I(z) > I_F,\\
0 & \text{otherwise}\end{cases}
\end{equation}

\noindent where $I(z)$ is given by eqn.~\eqref{eq:somacurrent}.
A similar functional form can be achieved by a non-volatile memristor with parasitic capacitance, as shown in eq.~\eqref{eq:MClimit} (see next section).
However other response functions, which might be easier to implement with memristors, can be used with NEF.
Other aspects of neural networks, such as critical behavior~\citep{Benna2016,Chialvo,GrossT} can be observed in networks of memristors~\citep{Avizienis}, although a theoretical understanding of their collective behavior is still poor for both systems~\citep{Caravelli2015,Caravelli2016,Sheldon2017}.

\subsection{Volatility: autonomous plasticity}
\label{sec:volatility}

Volatility is a key feature when processing information with memristors (in contrast to memory applications).
RC needs volatility to avoid trivial linear input-output mappings and NEF requires it to model the forgetting behavior of neurons.
There are many physical processes that can lead to a memristive volatile device, hence the source of volatility should be discussed in the context of a given technology.
In what follows we show how volatility, can be linked with a capacitance in parallel (parasitic) to a non-volatile device.
Consider an ideal series memristor-capacitor circuit~\citep{Joglekar2009} feed with a controlled current.
The memristor is modeled with eqns.~\eqref{eq:memristivedyn}, with $\alpha=0$, Kirchhoff's voltage law for this circuit gives:

\begin{align}
    R(w) \overbrace{\tder{q}{t}}^{I(t)} &=-\frac{1}{C} \overbrace{q(t)}^{\integ{I(t)}{t}} \rightarrow \tder{q}{t}(t)=-\frac{q(t)}{R(q(t))C}, \\
    R(q(t)) &= \Ron \left(1 + \frac{q(t)}{\beta}\right) + \Roff,
\end{align}

\noindent from which we obtain a limiting solution for $t \gg 1$

\begin{equation}
    q(t) = \frac{\beta}{\xi} W\left(\frac{\xi}{\beta} e^{-\frac{t}{\Ron C}} \, e^{-\frac{c_1}{\beta \Ron}}\right),
   \label{eq:MClimit}
\end{equation}

\noindent where $W$ is the product-log (Lambert) function~\citep{Veberic2010}, $c_1$ is an integration constant, and as before $\xi = \frac{\Roff-\Ron}{\Ron}$.
Eqn.~\eqref{eq:MClimit} shows that for large times the system has a typical exponential $RC$ circuit decay, which is shown in Fig.~\ref{fig:expstp} (left).
This behavior is observed in experiments~\citep{Berdan2016} where after an external stimuli an exponential-like decay is observed (see Fig.~\ref{fig:expstp} (right)).
%\FC{I've connected in words the equation above to the W lambert function below.}

\begin{figure}[htpb]
    \centering
    \includegraphics[scale=5]{ejp306200fig04.jpg}\includegraphics[scale=.6]{figures/indiveri.png}
    \caption{Left: theoretical profile of eqn. (\ref{eq:MClimit}).
    Right: Experimental profile of short-term plasticity with $TiO_2$.
Both figures are reproduced with permission from~\citep{Joglekar2009} and~\citep{Berdan2016}, respectively. }
    \label{fig:expstp}
\end{figure}

The limit of a normal $RC$ circuit can be obtained from the above in the limit $\beta\rightarrow \infty$, in which $\lim_{\beta\rightarrow \infty}\beta W(\beta^{-1} G(t))=G(t)$.
The response of a memristor-capacitor and an $RC$ circuit differs, with the first obtaining a longer retention than the first.
Also, the Lambert function $W$ has several properties of the logarithm function, and thus the response is similar to the one suggested in eqn.~\eqref{eq:MClimit}.
In the case $\alpha\neq 0$ for the model we described here, one has that  $w(t)=e^{t\alpha}\left(c_0+\frac{1}{\beta}\int_0^t d\tau e^{-t\tau} I(t)\right)$.
Thus, the correspondence between the parameter $w$ and the charge is not exact.
In addition, the parameter $\alpha \equiv \text{constant}$ is only an approximation.
The conductance of memristive devices decays when there is no input, and the rate of decay depends on the state of the memristor.
This is compatible with a state dependent parameter $\alpha$, rather than a constant~\citep[see Fig. 1 of][]{Ohno2011}.
A survey of recent hardware designs for temporal memory is provided in~\citep{SurveyTempNet}.

\subsection{Basis of computation}

As mentioned before, to design computers with memristors we need to understand and harness their natural computational power.
This is no simple task in general, but for networks of current-controlled memristors linear in an internal parameter, we can write down the differential equation describing the evolution of the memory states and obeying Kirchhoff voltage and current laws~\citep{Caravelli2017}:

\begin{equation}
    \tder{\vec{w}}{t}(t)=\alpha \vec{w}(t)-\frac{1}{\beta} (I+\frac{\Roff-\Ron}{\Ron} \Omega W)^{-1} \Omega \vec S(t),
    \label{eq:memnet}
\end{equation}

\noindent where $\alpha$ and $\beta$ are the parameters in eqns.~\eqref{eq:memristivedyn}-\eqref{eq:memristivestate}, $\Omega$ is a projector operator which depends on the circuit topology, $W_{ij} (t)=\delta_{ij} w_i(t)$ and $\vec S(t)$ is vector of applied voltages.
For arbitrary memristor components the generalization of eqn.~\eqref{eq:memnet} is not known.
In the approximation $\Roff= p \Ron$, with $p$ of order one, the equation above can be recast in the form of a (constrained) gradient descent~\citep{Caravelli2018}, which is reminiscent of the fact that the dynamics of a purely memristive circuit has an approximate Lyapunov function~\citep{Caravelli2018b,Caravell2018mfgen}.
In the simplified setting of purely memristive circuit without any other components it can be shown that these circuits execute Quadratically Unconstrained Binary Optimization~\citep{Boros2007}.
This idea is in general not recent, and it can be traced back to Hopfield~\citep{HopMemr1,HopMemr2,HopMemr3} for continuous neurons.
It is also not the only alternative approach to Hopfield networks using memristors~\citep{Hopmemror1,Hopmemror2,Hopmemror3,Hopmemror4}, as we will also  discuss it later.
