The present review aims at providing a structured view over the many areas in which memristor technology is becoming popular.
As in many other hyped topics, there is a risk that most of the activity we see today will dissipate into smoke in the coming years.
Hence we have carefully selected a set of topics in which we have experience, and in which we believe will remain relevant when memristors move out of the spotlight.
After a general overview on memristors, we provide a historical overview of the topic.
Next, we present an intuitive and a mathematical view on the topic, which we trust is needed to understand why anybody would consider alternative forms of computation.
Thus, experts in the fields might find this article slow-paced.

The memristor was introduced as a device that "behaves somewhat like a nonlinear resistor
with memory"~\citep{Chua71}, the resistance of the component depends on the history of the applied inputs: voltage or current.
Different curves of the applied input elicit a different dynamic response and final resistance of the memristor.
In addition, if we remove the input after certain time, leave the component alone, and come back to use it, the device will resume its operation from a resistance very similar to the one in which we left it; that is, they act as non-volatile memories.
Furthermore, memristors also react to the direction of the current, i.e. they have polarity.
This polarity is shown explicitly on the symbol of the memristor used in electronic diagrams: a square signal line (as opposed to a zigzag line for the resistor), inside an asymmetrically filled rectangle; as in Fig.~\ref{fig:symbol}.
\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.2\textwidth]{Memsymb.jpg}
    \caption{Electronic symbol of the memristor.}
    \label{fig:symbol}
\end{figure}
The interplay between the response behavior and the non-volatility of the device defines its usability either as a storage device or for more involved purposes such as neuromorphic computing.
The present article gravitates around the fact that memristors are electronic components which behave similarly to human neuronal cells, and are an alternative building block for neuromorphic chips.
Memristors, unlike other proposed components with neural behavior, can perform computation without requiring CMOS if not for readout reasons.

After the experimental realization of a memristor by~\citeauthor{Strukov2008}, which brought them to their current popularity, Leon Chua wrote an article titled "If it's pinched it's a memristor"~\citep{Chua2014}.
The title refers to the fact that the voltage drop across the device shows a hysteresis loop pinched to the zero of the voltage-current axes when controlled with a sinusoidal voltage or current.
The claim by~\citeauthor{Chua2014} is that a device which satisfies this property must necessarily be a memristor.
Chua also proved that such a device cannot be obtained from the combination of nonlinear capacitors, inductors and resistors~\citep{Chua71,Chua1976,Chua2014}.
It has been shown, however, that this property is a modeling deficiency for redox-based resistive switches~\citep{Valov2013}.

Resistive switching was of interest even before the 2008 article.
For instance the review of~\citeauthor{Waser2007b} shows that Titanium Dioxide had been in the radar for non-volatile memories for decades.
Nevertheless, one can identify a clear explosion of interest after the 2008 publication.
Next we give a list of established and new companies developing memristors technology using a variety of compounds.
The list is given without a particular order as these companies work on various type of compounds and type of memristors (for instance both ReRam and PCM).
Specifically, we are aware of: HP, SK Hynix, IBM, IMEC, Fujitsu, Samsung, SMIC, Sharp, TSMC, NEC, Panasonic, Macromix, Crossbar Inc., Qimonda, Ovonyx, Samsung, Intel, KnowM, 4DS Memories Ltd., Global Foundaries, Western Digital (before called SanDisk), Toshiba,  Macronix, Nanya, NEC, Rambus, ST Microelectronics, Winbond, Adesto Technologies Corporation, HRL Laboratories LLC, Elpida\footnote{We thank Magdalena E. Dale for helping us with the list of companies.}.

Going into the many physical mechanisms that make a memristive device work does require a deep knowledge of material properties.
For example, the hysteretic behavior can be cause by Joule heating, as shown in an example taken from macroscopic granular materials~\citep{Bequin2010}.
Also, hysteresis is a phenomenon which is common in nanoscale devices and it can be derived using Kubo response theory~\citep{DiVentra2013}.
Kubo response theory is used to calculate the correction to the resistance induced by a time dependent perturbation: this is the formal framework to address resistive switching due to either electrical, thermal or mechanical stresses in the material.
We can classify physical mechanisms that lead to memristive behavior in electronic components in four main types:
\begin{itemize}
    \item Structural changes in the material (PCM like): in these material the current or the applied voltage triggers a phase transition between two different resistive states;
    \item Resistance changes due to thermal or electric excitation of electrons in the conduction bands (anionic): in these devices the resistive switching is due to either thermally or electrically induced hopping of the charge carriers in the conducting band.
For instance, in Mott memristors the resistive switching is due to the quantum phenomenon known as Mott insulating-conducting transition in metals, which changes the density of free electrons in the material.
    \item Electrochemical filament growth mechanism: in these materials the applied voltage induces filament growth from the anode to the cathode of the device, thus reducing or increasing the resistance;
    \item Spin-torque: the quantum phenomenon of resistance change induced via the giant magnetoresistance switching due to a change in alignment of the spins at the interface between two differently polarized magnetic materials.
\end{itemize}
\noindent These mechanisms above are truly different in nature, and whilst not the only ones considered in the literature, are the most common ones.
We provide a technical introduction to these mechanisms in Appendix~\ref{sec:appphys} for completeness.

The primary application of memristors, as we will shown in this article, is towards neuromorphic computing.
The word neuromorphic was coined by Carver Mead~\citep{Mead1990} to describe analog circuits which can mimic the behavior of biological neurons.
In the past years the field has experienced an explosive development in terms of manufacturing neuromorphic novel chip architectures which can reproduce the behavior of certain parts of the brain circuitry.
Among the components used in neuromorphic circuits we consider memristors whose behavior resemble the one of a certain type of neurons.
The analogies between biological neuronal systems and electronic circuits are manifold: conservation of charge, thresholding behavior, and integration to mention just a few.
For instance, diffusion of calcium across the membrane can be mapped to diffusion in electronic components, and a computational role associated to the circuit design.
Also, it is known that the brain is power efficient: roughly twenty percent of an individual's energy is spent on brain activity, and this is roughly around \SI{10}{\watt}.
Besides their role in biological neural models (section~\ref{sec:memgalore}), we also discuss theoretical approaches to memristive circuits and their connection to machine learning (section~\ref{sec:memstorage} and~\ref{sec:memdataproc}).

The connection to machine learning is complemented with an overview on analog computation (section~\ref{sec:analogc}).
Historically, the very first (known) computer was analog.
It dates (approximately) 2100 years old, and it has been found in a shipwreck off the cost of the island of Antikythera at the beginning of the past century~\citep{antmec} (only recently it has been understood as a model of planet motion).
Despite our roots in analog computation, our era is dominated by digital computers.
Digital computers have been extremely useful at performing several important tasks in computation.
We foresee that future computers will likely be a combination of analog (or quantum analog) and digital (or quantum digital) computing chips.
At the classical level, several analog computing systems have been proposed.
Insofar, most of the proposed architectures are based on biological systems, whose integration with CMOS can be challenging, and this is the reason why analog electronic components are seen as promising alternatives~\citep{Adamatzky2016,Dalchau2018}.

Digital computation has been dominated by the von Neumann architecture in combination with CMOS technology.
Within this architecture we find two types of memory: Random Access Memory (RAM), a volatile and quick memory in which computation is performed, and non-volatile Hard Disk (HD) for long-term storage of information.
The neat separation between memory (RAM and HD) and computing (the processors or CPU) is a key features of computation using von Neumann architecture.
It requires that the data is split into packets, transfer to the CPU where computation is performed, and then repeated until the full computation task has been completed.
As far as our understanding goes, this separation is not present in the brain, in which memory and processing happen within the same units.
Several proposed architectures that mimic this property have been proposed, most of them based on memristors~\citep{DiVentra2013b}.
This type of computation is referred to as \textit{memcomputing}.

This article is organized as follows.
We first provide a historical introduction to memristors, as it is understood by the authors (sec.~\ref{sec:history}).
We then provide the key ideas behind the technology with simple mathematical models (sec.~\ref{sec:models}).
Albeit separated from the main text, we have provided an introduction to the main technology and physical principles underlying memristors in the Appendices~\ref{sec:appphys}.
We then focus our gaze on the description and use of memristors both for data storage (sec.~\ref{sec:memstorage}) and data processing (sec.~\ref{sec:memdataproc}): the former is the current target for marketing the technology, the second is believed to be the main application of memristors in the long run.
The similarity between memristors and neurons allows the implementation of machine learning on chip via Memristor/FPGA interfaces using crossbar arrays, we cover this topic in section~\ref{sec:crossbar}.
We have dedicated section~\ref{sec:analogc} to overview the fundamental topic of analog computation, followed by a brief recapitulation of machine learning techniques that can be seamlessly used with memristors (sec.~\ref{sec:reservoir}-~\ref{sec:NEF}).
We discuss in section~\ref{sec:memgalore} computation as an epiphenomenon of memristor dynamics and in connection with CMOS.
We close the article with some remarks that should help in the discussion of the topics covered.
