\subsection{Memristive computing}
\label{sec:memcomp}
In this section we review some works developing computation algorithms using networks of memristors and external control hardware based in crossbar arrays and FPGA.

In~\citep{Pershin2011,Pershin2013} it has been shown that memristive circuits can be used to solve mazes: connecting the entrance and exit of a maze, the memristive circuit as in Fig.~\ref{fig:maze} will re-organize (when controlled in DC) to allow the majority of the current to flow along the shortest path.
Although this phenomenon already occurs with regular resistances (with a diode in parallel), it is enhanced with memristors.
Memristors outside the shortest path go to their OFF state (high resistance) and the current difference (the contrast) to the active shortest path is augmented.
This example shows that the wiring between memristors and the asymptotic resistance values are deeply connected; this fact is reminiscent of the ant-colony optimization algorithms~\citep{Dorigo1997}, molecular computation~\citep{MolCop}, and other cellular automata models~\citep{Adamatzky1996}.

\begin{figure}[htpb]
    \centering
    \includegraphics{PershinDiVentraMaze.png}
    \caption{The maze-memristor mapping suggested in~\citep{Pershin2011}.}
    \label{fig:maze}
\end{figure}

Ideas along these lines can be pushed further in order to explore memristors as complex adaptive systems~\citep{prokopenko} able to self-organize with the guidance of the circuit topology and the control of external voltages.
Using eqn.~\eqref{eq:memnet} it is possible to obtain approximate solutions, for instance, of the combinatorial Markowitz problem~\citep{Caravell2018mfgen}.
Hybrid CMOS/Memristive circuits can, in principle, tackle harder problems via a combination of external control and self-organization~\citep{WilliamsSOM}.

In the literature several models of memristor-based architectures have been proposed.
Many of these proposals are based on the attractor dynamics of volatile dissipative electronics and inspired by biological systems.
For instance, a general theory of computing architecture based on memory components (memcapacitors, meminductors and memristors~\citep{memall}) has been introduced recently in~\citep{UMM}, called Universal Memory Machines (UMM), and shown to be Turing complete.
Similarly, an architecture based on memristors which includes Anti-Hebbian and Hebbian learning (AHaH) has been proposed in~\citep{ahah,ktram} for the purpose of building logic gates and for machine learning.
In both cases of UMM and AHaH the solutions of the problems under scrutiny are theoretically embedded in the attractors structure of the proposed dynamical systems, and have not been tested experimentally.

One way to show that a memristive system is a universal computing architecture is to break the system modularly into logic gates based on memristors, and show that the set of obtained gates is universal (which includes NOT and at least one of an AND or OR gates, as in DeMorgan's law~\citep{logicbook}).
Turing completeness follows from an infinite random access memory (the infinite tape).
Experimentally, it has been shown that it is possible to build logic gates with memristors (we mention for instance~\citep{galelog,loggatimp}).
An improvement upon this basic idea is to build input-output agnostic logic gates using memristors.
Any port of an agnostic gate can be used as input or output, and the remaining states of the gate will converge to the states of a logic gate, regardless of whether the binary variable is at the output or at the input of the gate.
For example, if the output of an agnostic AND gate is set to TRUE, the input variables will rearrange to be both TRUE; but if the output is FALSE, the inputs will re-arrange such as to contain at least one FALSE.
These are called Self-Organizing Logic Gates (SOLG) and it is suggested to use these to solve the max-SAT problem~\citep{TraversaPol,maxsat}~\citep[see also][and references therein]{diventratravrev}. Similar ideas were recently proposed using nanoscale magnetic materials rather than memristors~\citep{boospin}.

The two examples above show that memristors can be used both for analog computation, as in the case of shortest path problems, or to reproduce and extend the properties of digital logic gates.

\subsection{Natural memristive information processing systems: Squids, Plants, and Amoebae}

In recent years, and with the participation of L. Chua, there have been several reports re-interpreting models of natural information processing systems (neural networks, chemical signaling, etc.) in terms of memristors units.
Here we mention three examples: giant axon of squids~\citep{Sah2014}, electrical networks of some plants~\citep{Markin2014}, and Amoeba adaptation~\citep{AmLeao}.

\textbf{Squids.} Giant squids are model organisms big enough that they can be analyzed in detail at the singular cell level, and for which we posses a mechanistic model of the dynamics of their axons: the Hudgkin-Huxley (HH) model.
The HH model describes the voltage at the interface between synapses and dendrites, which is regulated by the flow of calcium and potassium.
The electrical circuit associated with this model is shown in Fig.~\ref{fig:hhmem}.
It entails the introduction of a nonlinear variable resistor for the calcium channel, and a linear variable resistance for the potassium channel and a capacitance~\citep{HHmodel}.
The equations of this model, when put in memristive form, are given by:

\begin{align}
i_{\text{K}}  &= \overbrace{g_{\text{K}} w_1^4}^{R_{\text{K}}^{-1}} V_{\text{K}} ,\\
i_{\text{Na}} &= \overbrace{g_{Na} w_2^3 w_3}^{R_{\text{Na}}^{-1}} V_{\text{Na}} ,\\
\tder{w_1}{t} &= \left( \text{K}_1\, V_{\text{K}} + \text{K}_2 \right) \left[e^{\,\text{K}_1\, V_{\text{K}} + \text{K}_2} - 1 \right]^{-1} \, \left( 1 - w_1 \right), \\
\tder{w_2}{t} &= \left( \text{Na}_1\, V_{\text{Na}} + \text{Na}_2 \right) \left(e^{\, \text{Na}_1\, V_{\text{Na}} + \text{Na}_2} - 1 \right)^{-1} \, \left( 1 - w_2 \right)  + \text{Na}_3\, e^{\, \text{Na}_4\, V_{\text{Na}} + \text{Na}_5} \, w_2,\\
\tder{w_3}{t} &= \text{Na}_6\, e^{\, \text{Na}_7\, V_{\text{Na}} + \text{Na}_8} \, \left( 1 - w_3 \right) - \left(e^{\, \text{Na}_1\, V_{\text{Na}} + \text{Na}_{9}} + 1 \right)^{-1} \, w_3,
\end{align}

\noindent where we see that a first order ($R_{\text{K}}$) and second order ($R_{\text{Na}}$) memristors are involved.
The parameters $\bset{\text{K}_i}$ and $\bset{\text{Na}_i}$ characterize the dynamics of the channels~\citep[see][for details]{Sah2014}.

The model above is a fit of the observed voltage data for the giant axon, and is useful in the analytical study of brain cell dynamics.
The proposed model allows the interpretation of synapses as circuits composed of rather nonlinear and non-ideal memristors.
That is, that memristors can be central in providing an alternative interpretation of a established model and further the understanding of biological neural information processing.

 \begin{figure}[htpb]
     \centering
     \includegraphics[scale=2]{HHmodel.png}
     \caption{The Hodgkin-Huxley model, with the variable resistances of the Sodium and Potassium channels interpreted as memristors.}
     \label{fig:hhmem}
 \end{figure}

\textbf{Plants.} In~\citep{Markin2014} three types of memristors models were developed and compared with the responses of some plants to periodic electrical stimulation.
The authors observed that in the studied plants the pinched hysteresis loop did not collapsed into a line for very high frequencies, as required for ideal memristors.
To recover this non-ideal behavior a parasitic resistor-capacitor pair was added in parallel to the ideal memristor model.
The general solution for their models is:

\begin{align}
i_m(t) &= \frac{e^{\beta t} V(t)}{\beta R_o\int_{0}^{t} h(V(x)) e^{\beta x} \ud x + A},\\
I &= i_m + i_{RC},
\end{align}

\noindent where $\beta$ is a parameter related to the time constant of the memristor, $V(t)$ is the driving periodic voltage, and $R_o h(V)$ is the memristance of a voltage-controlled memristor.
Depending on the model of the memristor considered, $h$ and the constant $A$ take different forms.
The total current $I$ is the observed magnitude, and $i_{RC}$ is the current through a series resistor-capacitor circuit in parallel with the memristor.
The study hints that memristive behavior is intrinsic to plants electrical signaling and that plant physiology could be better understood if memristors are considered as "essential model building blocks".

\textbf{Amoeba.} A memristive model of amoeba adaptation was introduced in the form of the simple circuit shown in Fig.~\ref{fig:aml}~\citep{AmLea,AmLeao}, and the model is simple enough that we can report it here.
Albeit the original article points to the concept of amoeba learning, we believe it is more appropriate to be addressed as a model of amoeba adaptation.
The memristor considered is a voltage-controlled memristor introduced in~\citep{WilliamsVCM}:

\begin{align}
    \tder{M}{t} &= f(V_M) \left(\theta(V_M) \theta(M - R_1) + \theta(-V_M) \theta(R_2 - M) \right),\\
    f(V) &= \frac{\beta - \alpha}{2} \left( \vert V + V_T \vert - \vert V-V_T \vert\right) -\beta V,
\end{align}

\noindent where $\theta(\cdot)$ is the Heaviside step function.

Because the inductance and the resistor in the circuit are in series, the same current $I$ flows through them.
The capacitor and the memristor are in parallel, hence their voltage drop are equal: $V_C = V_M$.
The conservation of voltage on the mesh implies $V_C + V_L + V_R = V(t)$.
We have $V_R = R I$ and $V_L = L I$.
The memristance $M(t)$ affects the voltage drop on the capacitor,

\begin{equation}
C V_C + \frac{V_C}{M(t)} = I.
\end{equation}

\noindent We thus obtain the three coupled differential equations:

\begin{align}
 \tder{I}{t} &= -\frac{R}{L}I + \frac{V - V_C}{L}, \\
 \tder{V_C}{t} &= -\frac{1}{M C}V_C + \frac{I}{C}, \\
 \tder{M}{t} &= f(V_M) \left( \theta(V_M) \theta(M - R_1) + \theta(-V_M) \theta(R_2 - R) \right).
 \label{eq:simam}
\end{align}

\noindent The stationary state requires that all time derivatives are zero.
In this state, the circuit is sensitive to new stimuli, i.e. it adapts.
For instance, in Fig.~\ref{fig:matam} we see the response of the system to new inputs, and new stationary states are obtained.
Albeit amoeba's adaptation is not as developed as in higher mammals, more general models entailing Pavloviav ``conditioning" have also been proposed in the literature using memristors~\citep{Pershin2010,Tan2017}.

\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{AmLearn2.png}
    \caption{The amoeba memristive learning model of~\citep{AmLea}.
(a) The circuit with capacitance $C$ and memristor $M$ in parallel (with resistance $R(t)$), and in series to a resistance $R$ and an inductance $L$.
(b) The function $f(V)$ for the memristor response in voltage.}
    \label{fig:aml}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{AmLearnMat.png}
    \caption{Simulation of eqn.~\eqref{eq:simam} and adaptation of the circuit to different stimuli.
We consider the parameters $\beta=100$, $\alpha=0.1$,
$R_1=3$, $R_2=20$, $C=1$, $R=1$, $L=2$, $V_t=2.5$ as in~\citep{AmLea}.
We stimulate the circuit with a square input $V(t)=0.5$ and frequency $\omega=10$ and then with reduce by a half the frequency, with $V(t)=-2$.
We see that the circuits ``adapts" to the new stimulus after a transient.
Initial conditions were $I_0=1$, $V_c^0=1, R(0)=7$ and used an Euler integration scheme with step $dt=0.1$.}
    \label{fig:matam}
\end{figure}

\subsection{Self-organized critically in networks of memristors}
We now consider the interaction between a high number of components with memory.
A common feature of large systems of interacting units with thresholds or discontinuous dynamics is critical behavior.
For example, self-organized criticality (SOC) is evinced when a dynamical system self-tunes into a configuration for which a qualitative change in the systems' behavior is imminent (e.g. a bifurcation).
These critical configurations are characterized by states with power law cross-correlation functions.
One of the main motivations of SOC is the explanation of power spectra of the functional form $P\sim \omega^\alpha$, with $-1<\alpha<-3$, in physical systems and in nature~\citep{Turcotte1999}.
The typical example is for instance the Gutenberg-Richter law of earthquakes, whose distribution of magnitude is Richter's law~\citep{Jensen1998,Markovic2014}.
The current characterization of the sufficient ingredients for SOC, however, is phenomenological: a system of interacting particles or agents in which thresholds are present and whose dynamics is dominated by their mutual interaction.
SOC has been suggested to be the underlying mechanism in the observed critical behavior of the brain~\citep{Chialvo}.
In this case, neurons can be interpreted as thresholding functions (logical gates) and it is thus tempting to interpret the criticality of the brain as a SOC phenomenon.

SOC can be produced using large networks of memristors: atomic switch networks are dominated by the interaction due to Kirchhoff laws~\citep{Avizienis}, and thus the observed criticality seems intuitively (power law distribution of the power spectra, for instance) to be connected to a SOC-type~\citep{Sheldon2017} phenomenon because of the rather nonlinear and threshold-like behavior of memristors.
It is however easy to observe that power law distributions in power spectra can be obtained in a rather simple way as follows~\citep{Caravelli2017}.
Consider a system of interacting memristors, whose linearized dynamics close to a fixed point obtained with DC voltage stimulation (i.e. saturated memristors: resistors) is written as:

 \begin{equation}
     \tder{\vec{w}(t)}{t}= A \vec{w}(t).
 \end{equation}

\noindent The matrix $A$ is a non-trivial combination of voltage sources and projectors on the subspace of the circuit's graph~\citep{Caravelli2018}.
We divide the spectrum of $A$ in positive and negative eigenvalues, and the distribution $\rho_+(A)$ and $\rho_-(A)$.
Since $0\leq w_i(t)\leq 1$, we look at the average relaxation $\langle w(t)\rangle=\sum_{i} \frac{ w_i(t)}{N}$, which can be written as

\begin{equation}
   \langle w(t)\rangle= \frac{1}{N}\sum_i \left( \sum_j (e^{A t})_{ij} w_j^0 \right)=\frac{1}{N}\trace\left(e^{At} W_0\right)=\frac{1}{N}\trace\left(e^{\lambda^+t} \tilde W^0+e^{\lambda^-t} \tilde W^0\right),
\end{equation}

\noindent The positive part of the spectrum will push memristors to the $w=1$ state, while the negative part to the $w=0$ state.
We can write the trace on each positive and negative state as:

\begin{equation}
    \frac{1}{N}\trace(e^{-\lambda^{-}_i t} \tilde w_i)=\int d\lambda \rho^{-}(\lambda) e^{-\lambda t} \langle w_i^0\rangle = \frac{1}{2}\int d\lambda \rho^{-}(\lambda) e^{-\lambda t},
\end{equation}

\noindent if the memristors are randomly initialized.
As it turns out, if $\rho^{-}(\lambda)$ is power law distributed, then $\langle w(t) \rangle \approx t^\gamma$.
From this, we observe that the power spectrum distribution is of the form $P(\omega)\approx \omega^{-(1-\gamma)}$, from which a ``critical state" is obtained.
It was shown in~\citep{Caravelli2017} that if the circuit is random enough, numerical simulations produce $\gamma\approx -1$.
A similar argument can be obtained for $\rho^+(\gamma)$ with the transformation $w\rightarrow 1-w$.
This result, when using networks of HP-memristors, is in line with what is experimentally observed in~\citep{Avizienis}.

The phenomenon above is not classified as SOC, since only a certain matrix $A$ is required to obtain it.
This implies that unless thresholding is present, the criticality observed is not self-tuned, but it might be due to the complex interconnections.
This is not the case if, however, the memristors themselves have voltage induced switching~\citep{Sheldon2017}.
In this case, the criticality is due to the a SOC-like phenomenon which had been already observed in random fuse networks, which is described by a percolation transition~\citep{Alava2006}.

\subsection{Memristors and CMOS}
\label{sec:experiments}
The idea of using variable resistances in order to implement learning algorithms is not new.
As we have seen, crossbar arrays were introduced already as early as 1961~\citep{Steinbuch1961}.
The idea of using instead variables resistances precedes the paper of Steinbuch by one year, and was introduced by Widrow~\citep{Widrow1960} in order to implement the Adaline algorithm explained below.
The ``\textbf{mem}istor", a name extremely similar to the one of ``\textbf{memr}istor" introduced ten years later, was a current-controlled variable resistance~\citep{Adhikari2014}.
This limited the ability to package a huge number of memistor synapses.
For (modern) machine learning applications, however, it is necessary to implement learning rules with a large number of neurons and synapses.
As we have seen, memristors are the equivalent component for a synapse~\citep{memsynapses}.
The neuron is instead the biological equivalent of a N-port logic gate (threshold function).
For more general applications a crossbar-array like packing is desirable.
There are many ways of using memristors for applications in neural networks~\citep{reviewGorm}.
For instance, the circuit proposed in Fig.~\ref{fig:nnMM} provides a simple circuit which has a linear output neuron controlled by a resistance $R$, without threshold.
The threshold can be however easily introduce via a Zener diode.
In order to understand why memristors do not have necessarily an advantage over digital implementations of neural networks, we follow the argument of~\citep{DeBenedictis2016} to understand the energy efficiency.
Using Landauer theory, the energy dissipated by a digital gate is $E_{gate}\approx -2 \log(p_{err}) k T$, where $p_{err}$ is the probability of an error.
For the analog implementation above, the only dissipation is due to Johnson-Nyquist noise in the amplifier, which is of the order $4 kT f$, with $f$ the amplifier's bandwidth and $N$ the number of synapses.
Keeping track of error and number of bit precision $L$, one reaches the conclusion that
\begin{align}
    E_{dig}&\approx 24 \log(\frac{1}{p_{err}}) \log_2^2(L) N k T,\\
    E_{memr}&\approx \frac{1}{24} \log(\frac{1}{p_{err}}) L^2 N^2 k T,
\end{align}
which scales in the number of artificial neurons in favor of digital implementations.
This surprising result thus confirms that the devil is in the detail, and that not necessarily all analog implementations of digital systems are better.


 \begin{figure}
    \centering
    \includegraphics[scale=1.5]{CMOSadder.png}
    \caption{Memristor equivalent of a neural network with three neurons and two synapses, with an output amplifier.}
    \label{fig:nnMM}
\end{figure}

The architecture of Fig.~\ref{fig:nnMM} however does not take advantage of the scalability of crossbar arrays which we have mentioned earlier, in terms of number of neurons, and other implementations might be more energy efficient.
For instance, in~\citep{Alibart2013} first experimental results of memristive technology for pattern classification on a 3x3 image matrix was studied using crossbars and $TiO_2$ memristors.
In general, learning using crossbars follows a general weight update strategy.
For regressions, current-controlled memristors can be used with a simple serial architecture~\citep{Carbajal2015} while unsupervised learning can be performed by implementing the K-means algorithm~\citep{KMeansmemr,UnsLern,Jeong2018}.

\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{FeedbackCrossbar.png}
    \caption{Feedback loop and learning in crossbar arrays.}
    \label{fig:feedbackcrossbar}
\end{figure}

Next, we focus on applications of memristor technology for Machine Learning (ML).
Algorithms like backpropagation on conventional general-purpose digital hardware (i.e., von Neumann architecture) are highly inefficient: one reason for this is the physical separation between the memory storage (RAM) of synaptic weights and the arithmetic module (CPU), which is used to compute the update rules.
The bus between CPU and RAM acts as a bottleneck, commonly called von Neumann bottleneck.
As mentioned before, the idea is thus to use memristive based technology to introduce computation and storage on the same platform.
One way to introduce learning into crossbars is by introducing feedback into the system, as in Fig.~\ref{fig:feedbackcrossbar}.

Let us consider a discretized dynamics, and call $W^k$ the weight matrix in the crossbar array at time step $k$.
A general update is of the form

\begin{equation}
    W^{k+1}=W^{k}+ f(W^k, Q),
    \label{eq:wupdate}
\end{equation}

\noindent where $f(W^k)$ is a certain function of the weights and some ancillary variables $Q$ (which could be training data, inputs, outputs, etc).
For instance, in the case of neural networks training (gradient descent type),
$f(W^k)=- \eta \nabla_{W^k} \Vert\vec t-\vec o\Vert^2$
where $\vec t$ is the output we aim to obtain (given the inputs), and $\vec o$ is the output.
For resistive crossbars, we have seen that $\vec o=G(W^k) \vec v$ is linear in the inputs and $G$ is the conductance, while $\eta$ is a time scale parameter.
If $f(W)_{mn}=\eta x_{m}^k x_{n}^k$, where $x_{n}^k$
 is the teacher inputs (patterns) indexed by the index $k=1,\cdots, K$, then the Hebbian learning rule is called adaline algorithm~\citep{Hebb1949,Widrow1990}.
 From the point of view of circuit theory, the feedback can be introduced via CMOS-Memristor integration.
For instance, in~\citep{Soudry2015} one way to perform online learning with an adaline algorithm using metal-oxide-semiconductor field-effect transistors (MOSFETs) has been provided.


Models like the one just described can, for instance, be used for sparse coding~\citep{Rozell2008}.
Sparse coding can be mathematically formulated as a problem in linear algebra.
Consider a vector which describes a certain quantity of interest $\vec x$ (for instance, an image), and a dictionary which is available as $\vec \phi^i$, $i=1,\cdots,M$.
Further, assume that $\phi^i$ and $\vec x$ belong to $\mathbb{R}^N$.
If $M>N$ and the vectors are independent, we can always find the coefficients $a_i$ such that $\sum_i a_i \vec \phi^i=\vec x$, and there is an infinite number of ways to do this expansion.
The goal of sparse coding is solving the following optimization problem:

\begin{align}
\vec x&=\sum_{i=1}^M a_i \vec \phi^i, \\
\text{min}_{\vec a}& \Vert\vec a\Vert_0,\ \Vert\cdot\Vert\text{ 0-norm},
\end{align}

\noindent and which is notoriously NP-hard. The problem above can be relaxed by replacing the 0-norm with the 1-norm, and a system of differential equations for continuous neurons can be implemented in crossbar arrays.
In general, these are equations of the type
\begin{equation}
    \tder{\vec{u}(t)}{t}=F(\vec{u}(t),\vec{q}(t)),
\end{equation}
where $u_i(t)$ and $q(t)$ are some control functions and $F(\cdot)$ represents a generic continuous function. Some details are provided in the Appendix \ref{sec:scod}. The variables $u_i(t)$ are intended as the memory elements in a memristor:
in a crossbar array system, the equations above have been implemented by combining a field programmable gate array (FPGA) with a crossbar in~\citep{Sheridan2017}, where using a threshold function $T_\lambda(x)=x$ if $x>\lambda$ and zero otherwise.
Another update rule used in experiments is Sanger's update rule~\citep{Sanger1989}, defined as

\begin{equation}
    W^{k+1}=W^k+2 \eta \vec{o}^t ( \vec{x}-(2W^k -I)\vec{o}),
\end{equation}

\noindent where $\vec{o}$ is the output of the crossbar array, and $\vec{x}$ the input, which is used in~\citep{Choi2015} in order to perform PCA, again with the use of FPGA.

Recent advances in Deep Neural Network implementations using memristor technology (PCM) show remarkable efficiency in terms of energy consumption for a given constant accuracy, with gains up to two order of magnitudes when compared to GPU implementations~\citep{Hopmemror3}.
The architecture used therein is not dissimilar from the one described in this section using a combination of CMOS and crossbar arrays.
This is another example of how crossbar arrays are amenable to efficient matrix multiplication, which renders them competitive to other GPU implementations~\citep{Hopmemror4,HopMemr1,HopMemr2}.

We have already discussed reservoir computation (RC)~\citep{Carbajal2015,Du2017} as another way of using memristors within the framework of machine learning whilst taking advantage of their temporal dynamics (see sec.~\ref{sec:reservoir}).
In~\citep{Du2017}, RC was implemented with a memristor reservoir and one layer output with 32x32 crossbar of memristors and trained using an external FPGA.
The model was then used successfully to classify the MNIST dataset (5x4 images) as a proof of principle application.
The advantage of using RC is that it can be implemented both for online learning and for classification in a teacher-signal framework, and with relatively little computational effort.

Concluding, CMOS provides an advantage for controlling memristive circuits, but it is possible to use just the inner dynamics of memristors to perform learning~\citep{Caravelli2018}.

The main question that remains unanswered is whether analog system have an advantage over digital ones at all.
A strong argument is provided by~\citeauthor{Vergis1986}, where it is shown that analog devices can be simulated with polynomial resources on a digital machine with enough resources.
We pointed out the importance of the collective properties of memristive circuits.
The dynamics of a collection of memristors interacting on a circuit can, in principle, derived from the implementation of circuit voltage and current constraints, and strongly depends on the dynamics of a single unit.
Understanding the interaction of memristors via Kirchhoff laws can in principle enable the application of memristors to a variety of computational tasks.
Below, we make this more precise using a general mapping from digital to analog computation.
We consider a differential equation of the form:

\begin{equation}
    \tder{\vec{y}}{t}(t) = f(\vec{y}(t),t), \quad y(t_0)=y_0,\quad
\end{equation}

\noindent which describes a physical system.
In~\citep{Vergis1986}, a constructive proof based on Euler integration method is provided, and it is shown that the amount of resources needed to simulate the system above on a digital machine is polynomial in the quantities $R$ and $\epsilon$:

\begin{equation}
    R=\max_{t_0\leq t \leq t_f} || \tder{{}^2\vec{y}}{t^2}(t)||,\; \epsilon = ||\vec{y}(t_f) - \vec{y}^*||,
\end{equation}

\noindent where $\vec{y}^*$ is the simulated system, and thus $\epsilon$ is our required precision.
Since for quantum systems $R\propto 2^N r^*$, and $r^*$ is a constant, classical computers require an exponential amount of resources to simulate a quantum physical systems.
This does not mean that classical systems can always be simulated: if the second derivative is large, our system requires a lot of computational power to be simulated on a digital machine.
A similar argument applies also to quantum computers, on which there has been a huge effort in the past decades.
In the case of a quantum system with $N$ qubits, the vector $\vec{y}(t)$ is $2^N$ dimensional according to the Schr\"{o}dinger equation.
In a typical (analog) electronic computer,  $\tder{{}^2 \vec{y}}{t^2}$ is the derivative of the voltage.
Thus, if our circuit presents instability, it is generically hard to simulate the system.
This is specially striking in the case of chaotic behavior, which is known to emerges when a dynamical system is connected to a hard optimization problem~\citep{ErcseyRavasz2011}.
These are also arguments against the simulation of memristive system on a digital computers, which do not apply to the actual physical system performing the analog computation.
