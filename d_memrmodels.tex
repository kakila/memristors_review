In this section, we provide a general mathematical description of memristive systems followed by the details of the memristor model with linear memory dynamics proposed by~\citeauthor{Strukov2008}.
This is one of the simplest memristors models that captures the behaviors relevant for technological applications of memristive systems.

The fundamental passive electric components are the resistors, capacitors and inductors.
These components couple the voltage difference $v$ applied between their ports with the current $i$ flowing through, with the following differential relationships:

\begin{gather}
  \begin{aligned}
    \text{Resistor:}  & \quad \ud v=R \, \ud i;\\
    \text{Capacitor:} & \quad \ud q = C \, \ud v, \quad \text{charge: } \ud q = i \, \ud t ;\\
    \text{Inductor:}  & \quad \ud\Phi=L \, \ud v, \quad \text{flux: } \ud \Phi = v \, \ud t.
  \end{aligned}
\end{gather}

\noindent These relations introduce the resistance $R$, the capacitance $C$, and the inductance $L$.
The ideal memristor was initially and abstractly formulated as the flux-charge relationship:

\begin{equation}
    \ud \Phi = M\, \ud q,
\end{equation}

\noindent which, in order to be invertible, must have $M$ defined either in terms of $\Phi$, or $q$, or it must be a constant.
The latter case coincides with the resistor, while if $M$ is defined in terms of $\Phi$, then we have a voltage-controlled memristor; if it is defined in terms of $q$ it is a current-controlled memristor\footnote{For the sake of simplicity we do not make a difference between current-controlled and charge-controlled, this is an abuse of the language.}.
The units of the coupling $M$ are Ohms, as can be seen by the units of the quantities it relates, which justifies the notion of memristor at least as a \textit{nonlinear resistor}.
However, according to~\citeauthor{Chua2014}, only nonlinear resistors showing a pinched hysteresis loop, i.e. $V=0 \rightarrow I=0$, are classified as memristors.
An example is shown in Fig.~\ref{fig:pinchys}.
Further classifications~\citep[ideal, extended, generic, see]{Chua2014} can be added to a device depending on other properties of the current-voltage ($I-V$) diagram that are observed experimentally when the device is controlled with a sinusoidal voltage at a certain frequency.
For example, if for high frequencies the $I-V$ becomes a straight line (recovery of pure linear resistance, no hysteresis) the device is a generic memristor, under~\citeauthor{Chua2014}'s nomenclature.
General memristors might be locally-active, showing regions where the resistance changes sign~\citep{Chua2015}.

Many systems, not necessarily electronic ones, can fulfill these properties.
Physical systems with memristor-like behavior are denominated memristive systems, and are described by the following non-autonomous (input-driven) dynamical system,

\begin{gather}
  \begin{aligned}
    \tder{\bm{x}}{t} &= F(\bm{x}, u),\\
    y &= H(\bm{x}, u) u,
  \end{aligned}
  \label{eq:eqmem}
\end{gather}

\noindent where $\bm{x}$ is a vector of internal states, $y$ a measured scalar quantity and $u$ a scalar magnitude controlling the system.
The pair $(y,u)$ is usually voltage-current or current-voltage, hence the second equation is simply Ohm's law for the voltage-current relationship.
In the first case (current-controlled system) the function $H(\bm{x},u)$ is the called memristance, and it is called memductance in the second case (voltage-controlled).
The function $H(\bm{x},u)$ is assumed to be continuous, bounded, and of constant sign\footnote{In order to rule out locally active devices, it is necessary to further require that $H(x,u)$ is monotonic in $u$.}, leading to the zero-crossing property or pinched hysteresis: $u = 0 \Rightarrow y = 0$.
The states $\bm{x}$ can be many physical states other than charge, e.g. the internal temperature of a granular material.
The minimum number of internal states on which $H(\bm{x},u)$ depends is called the \textit{order} of the memristive system.

The reader should not overlook the difference between an ideal memristor (a mathematical model), memristive systems (also a mathematical model), and a memristor (a physical device).
Ideal memristors are models of devices in which the internal parameter controlling the resistance is the charge, which is linked to the flux.
Memristive systems generalize this behavior, and the internal parameters can be other physical quantities, or combination of physical quantities, even if they do not include flux or charged particles.
The latter is better attuned to many memristors, i.e. physical realizations of memristive behavior.

Among the (large) class of memristive systems, the model proposed by~\citeauthor{Strukov2008} is appealing due to its simplicity.
This model, shown in eqns.~\eqref{eq:memristivedyn}-\eqref{eq:memristivestate}, captures the basic properties of memristors that are relevant for the understanding of the device and for its technological applications.

Eqns.~\eqref{eq:memristivedyn}-\eqref{eq:memristivestate} model the behavior of a current-controlled Titanium Dioxide memristor~\citep{Strukov2008}, also known as the HP-memristor:

\begin{align}
    \tder{w(t)}{t} &= \alpha w(t)-\frac{1}{\beta} I(t), \label{eq:memristivedyn}\\
    \frac{V(t)}{I(t)} &= R\left(w(t)\right) := \Ron \left[1 - w(t)\right] + \Roff w(t) \label{eq:memristivestate}\\
    w(0) &= w_0 \rightarrow R(w_0) = R_0, \quad  0 \leq w(t) \leq 1,
\end{align}

\noindent where $I(t)$ is the current flowing in the device at time $t$.
The quantities $\Ron \leq \Roff$ in the parametrization of the resistance $R$ in terms of the adimensional variable $w(t)$ (called \textit{memory}), define the two limiting resistances.
The parameters $\alpha$ (with units \si{\second^{-1}}) and $\beta$ (with units \si{\second^{-1}\ampere^{-1}}), define the dynamic properties of the memristor.
The parameter $\alpha \geq 0$, sometimes called \textit{volatility}, indicates how fast the resistance drifts towards $\Roff$ in the absence of currents~\citep{volc}.
Since the resistance function depends on a single state $w$, this is a first order memristive system.
Albeit this model has several drawbacks when it comes to its connection to the physical behavior of ion migration in the conductor~\citep{DiVentra2013,Abraham2014,Vongehr2015,Abraham2016,Abraham2018a,Abraham2018,Tang2016}, it will be the reference for most derivations and discussions in this article, as it captures several key properties of a generic memristor.

We begin with the case with $\alpha=0$, i.e. a non-volatile memristor.
We solve the system of equations above using a zero mean small amplitude driving voltage $V(t)$ which is such that for all times $0< w(t)< 1$, i.e. the memristor should not saturate at any time~\citep{Wang}:

\begin{equation}
    R\left(w(t)\right)\tder{w(t)}{t}=\tder{}{t} \left( (\Roff-\Ron) \frac{1}{2} w^2(t)+ \Ron w(t) \right)=- V(t),
\end{equation}

\noindent from which we derive, if we define $\xi=\frac{\Roff-\Ron}{\Ron}$ and integrate over time:

\begin{equation}
 \left( \frac{\xi}{2} w^2(t)+ w(t)\right)= \left( \frac{\xi}{2} w^2(t_0)+ w(t_0)\right)+ \int_{0}^t V(\tau) \ud \tau, \label{eq:periomemsol}
\end{equation}

\noindent whose solution is given by

\begin{equation}
    w(t)=\frac{\sqrt{2 \left(\frac{\xi}{2} w^2(t_0)+ w(t_0)\; + \; \int_{0}^t V(\tau) \ud \tau\right) \xi +1}-1}{\xi }.
\end{equation}

\begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{HPmemloop.png}
    \caption{Pinched hysteresis loop of a HP-memristor.
    The parameters of the model are $\alpha = 0$, $\beta = \SI{0.3}{\milli\ampere}$, $\Ron = \SI{1}{\kilo\ohm}$ and $\Roff = \SI{6}{\kilo\ohm}$.
    The driving voltage is $V(t) = \sin (2 \pi f t)$ with $f$ taking several values.
    We see that for increasing frequencies the hysteresis is reduced, eventually converging to a  line (resistor).
    The loop slope (dashed lines) decreases with increasing frequency.}
    \label{fig:pinchys}
\end{figure}

This equation fulfills the fundamental property of a memristor: it has the zero crossing property (pinched hysteresis loop).
Furthermore, it shows properties of generic memristors: the loop tilts to the right for driving signals with increasing frequencies, becoming a straight line for sufficiently high frequencies.
To see the latter consider $V(t) = V_0 \cos \left(\omega t\right)$, as the frequency $\omega\rightarrow \infty$.
For this voltage the integral in eq.~\eqref{eq:periomemsol} goes to $0$, and $w(t)\rightarrow w(t_0)$.
This implies a \textit{constant} resistance, i.e. the memristor becomes a resistor for high frequencies.

In this model for $\alpha=0$, we have that $w(t)\sim q(t)$, where $q(t)$ is the charge in the conductor.
For a titanium dioxide thin film, it was shown in~\citep{Strukov2008} that

\begin{equation}
    \beta^{-1} \sim \frac{\mu_e \Ron}{D},
\end{equation}

\noindent and thus

\begin{equation}
R(q) \approx \Roff \left( 1 - \frac{\mu_e \Ron}{D^2} q \right).
\end{equation}

\noindent Where $\mu_e$ is the electron mobility in the film, $\Roff$ is the resistance of the undoped material (for instance titanium oxides, $\sim \SI{100}{\ohm}$), and $D$ is a characteristic length of the film.
From the micrometer to the nanometer the value of $\beta$ grows by a factor of $10^6$, which is why the memristance in this material is a nanoscale effect.

Memristors are also referred to as resistive switches, because when $\frac{\Roff}{ \Ron}\gg 1$ and $\beta$ is small enough, the memristor can be quickly driven from one state to the other via a current (or voltage) inversion.
The operation is often referred to a $SET$ or $RESET$ in the technical literature, depending on the operation one is interested in, and it allows the use of non-volatile memristors as bits.

In the volatile case, i.e. $\alpha > 0$, the memristor does not retain its memory state.
When a volatile memristor is not activated via an external forcing, the memristor drifts to its insulating state, $R(t=\infty)=\Roff$.
That is, if $I(t)=0$, then the internal memory is simply given by $w(t)= w_0 e^{\alpha t}$, where $w_0$ is the initial condition.
This behavior was first experimentally observed in~\citep{Ohno2011}.
Volatility is discussed again in Section~\ref{sec:volatility}.

Following the distinction between memristive and memristors we have discussed, when $\alpha \neq 0$ the parameter $w$ cannot be uniquely identified with the charge, hence the model does not correspond to a bona fide memristor.
In fact, for a single component the solution of:

\begin{equation}
    \tder{w(t)}{t}=\alpha w(t)-\frac{1}{\beta} \tder{q(t)}{t}
\end{equation}

\noindent is given by

\begin{equation}
    w(t)=\frac{e^{\alpha  t}}{\beta} \int_1^t e^{-\alpha  \tau } \frac{d}{d\tau}q(\tau ) \, d\tau +w_0 e^{\alpha  t}
\end{equation}

\noindent which is a composite function which includes the charge.


The numerical model of the memristor allows us to study their theoretical capabilities as well as simulating large networks of these devices.
From the point of view of numerical simulations, the dynamics at the boundaries of $w$ need to be stable, or alternatively, $w$ be constrained in $[0,1]$.
In general the latter does not scale well in terms of runtime, and for this reason it is often useful to bound the internal states of the memristor model via the introduction of window functions~\citep{Pershin13ventra, Biolek2009, Biolek2013, Biolek2015, Nedaaee2011, Strukov2008, Joglekar2009}.
Since we are not aware of any systematic validations of windowing functions with real devices, we believe they should be considered tricks useful for large simulations.
It is common to use windowing techniques based on polynomials; for a extensive review we refer the reader to~\citep{Biolek2015}.
Polynomial windowing functions that reproduce nonlinear dopants drift can be introduced via the so-called Joglekar window function $F(x)$~\citep{Joglekar2009, Biolek2009}, such that $F(1)=F(0)=0$, which generalizes the evolution of $w(t)$ as

\begin{equation}
    \tder{w(t)}{t} = \alpha w(t) - F(w) \frac{1}{\beta} I(t),
\end{equation}
\noindent where the window function can take the form $F(w) =1 - (2 w - 1)^{2p}$, with $p$ a positive integer.
Depending on the type of memristor model, different window functions might provide a better approximation of the nonlinear effects near the boundaries of the memory.
For an overview of the various physical mechanisms that can be involved, we refer the reader to the Appendix~\ref{sec:appphys}.

Numerical simulations of memristive networks can be achieved with different methods.
Efficient ones include the SPICE methodology~\citep{spicememr,spicememr2} and the more recent Flux-Charge method~\citep{fluxcharge}.
Furthermore, general solvers for Differential-algebraic system of equations (e.g. SUNDIALS solvers~\citep{hindmarsh2005sundials}) can also be used for networks with a few hundred components, without major loss of performance.
For some networks (without capacitance or inductance), it is possible to derive a monolithic system of ordinary differential equations for the evolution of the network states, which already include the nonlocal algebraic constraints imposed by Kirchhoff's laws~\citep{Caravelli2017,Caravelli2017b}.
The choice of the method is largely based on the questions to be addressed by the simulations, which often results in a trade-off between generality of the solver (e.g. flexible memristor model) and performance.
