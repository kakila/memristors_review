A memristor can be used as a digital memory of at least one bit.
The simplest way to achieve this is to use the memristor as a switch.
If the memristor is non-volatile we can set its memristance to the value $\Ron$ or $\Roff$ using a voltage or current pulse, and associate these resistances with the state of a bit.
We stress that non-volatility is essential for memory applications, because a volatile memristor, i.e. one that drifts back to a high resistance state autonomously, would require a permanent source of current/voltage to keep it in the low resistance state.
Volatility will, in general, render memristive memory worthless in terms of energy consumption.
This is one of the reasons why volatility is engineered out of the devices meant for storage applications.

In order to illustrate the mechanism of flipping a bit, consider again the non-volatile memristor model described by eqn.~\eqref{eq:memristivedyn} (i.e. $\alpha = 0$) connected to a constant voltage source $V_\text{write}$.
Solving the differential equation for $w(t)$ gives:

\begin{equation}
    w(t) = \sqrt{ a + b V_\text{write} t},
    \label{eq:setwrite}
\end{equation}

\noindent where the coefficients $a,b$ depend on the parameters, but not on the input voltage.
Thus, by controlling the sign of the input voltage $V_\text{write}$ we can switch the resistance from $\Roff$ to $\Ron$ and vice versa (the flip of a bit).
The switching happens within a characteristic time $\tau$:

\begin{equation}
\sqrt{ a + b V_\text{write} \tau} = 1 \rightarrow \tau \leq \frac{1}{b V_\text{write}}.
\end{equation}

\noindent Hence, to flip the bit we need to apply $V_\text{write}$ for at least this amount of time.
To read the bit, we apply a voltage $V_\text{read} \ll V_\text{write}$ (and optionally for period of time shorter than $\tau$) and compute the resistance via the measurement of the resulting current.

Eqn.~\eqref{eq:setwrite} applies only to the idealized memristor described by the model in eqns.~\eqref{eq:memristivedyn}-\eqref{eq:memristivestate}.
This model might not be valid for real devices which will show a different dynamic response to input voltages or currents.
However, the idea of controlling and reading a memristor bit with pulsed inputs remains the same.
Fig.~\ref{fig:resetset} shows the response of a real $TiO_2$ memristor to write voltages (SET and RESET).

\begin{figure}
    \centering
    \includegraphics{figures/resetset.png}
    \caption{Reset and set for $TiO_2$ with pulse within the hundred of microseconds. Reproduced with permission from~\citep{Mostafa2015}}
    \label{fig:resetset}
\end{figure}

The fact that it is possible to write and read the state via signal pulses allows for advantageous scaling of power consumption and bit density~\citep[see][for details]{Linn2016}.
As we discuss below, it is possible to use crossbar arrays with memristors to increase the density of components.
The density of components in crossbar arrays scales as $\frac{1}{4\ell^2}$, where $\ell$ is set by the length scale of optical lithography (a few nanometers).
The reported state of the art as we write this article is roughly \SI{7}{\giga B\per\milli\metre\squared}, and with writing currents of \SI{0.1}{\nano\ampere}~\citep{Shuang2018}.

Another challenge for storage based applications, besides volatility, is device variability, e.g. the variation of properties like the switching time $\tau$ for devices built under similar conditions.
Current research in oxides focuses on these variability aspects, how to standardize the production of memristors, and the optimization of properties like the switching and retention time, and the durability of each singular device.
The status of the technology for memory storage for different type of devices is presented in Tab.~\ref{tab:techlevel}.

\begin{table}[htpb]
    \centering
    \begin{tabular}{c|c|c|c|c|c|c|}
                                   & Memristor & PCM & STT-RAM & DRAM & Flash & HD \\
                                   \hline
         Chip area per bit ($F^2$) & 4 & ~10 & 14-64 & 6-8 & 4-8 & n/a   \\
         Energy per bit (pJ) &  0.1-3 & $10^{1-2}$ & 0.1-1 & $10^0$ & $10^3$ & $10^4$ \\
         Read time (ns) & <10 & 20-70 & 10-30 & 10-50 & ~$10^{4-5}$ & ~$10^4$ \\
         Write time (ns) & 20-30 & ~$10^{1-2}$  & ~$10^{1-2}$ & $10^1$ &  $10^{5}$ & ${10^6}$ \\
         Retention (yrs) & 10 & 10 & $10^{-1}$ & $10^{-5}$ &  10 & 10 \\
         Cycles endurance & $10^{12}$ & $10^{7-8}$ & $10^{15}$ & $10^{17}$ & $10^{5-8}$ & $10^{15}$ \\
         3D capability & yes & no & no & no & yes & n/a
    \end{tabular}
    \caption{Characteristics of various storage devices (for details see~\citep{Meena2014}).
The table compares to memristor technology to competitors like PCM and more standard devices based on STT, DRam, Flash and HD.}
    \label{tab:techlevel}
\end{table}

\subsection{Crossbar arrays}
\label{sec:crossbar}

In this section we briefly review the crossbar array architecture used in memristor based storage and its application in artificial neural networks.

Crossbar arrays are based on the architecture depicted in Fig.~\ref{fig:steinbuch}.
The figure shows an array composed of horizontal (\textit{e-lines}) and vertical (\textit{b-lines}) lines that are initially electrically isolated from each other.
A 2-terminal component, e.g. a memristor, is connected across each pair of vertical and horizontal lines.

\begin{figure}
    \centering
    \includegraphics[scale=0.4]{figures/Steinbuch.png}
    \caption{Learning matrix, introduced by Steinbuch~\citep{Steinbuch1961}.Reproduced with permission from~\citep{Kohonen1989}.}
    \label{fig:steinbuch}
\end{figure}

In order to operate crossbar arrays, a voltage $\xi_j$ is applied to the $j$-th e-line, and an another voltage $\eta_i$ to the $i$-th b-line.
A memristance $M_{j,i}$, placed across $j$-th e-line and the $i$-th b-line, is controlled by the voltage $\xi_j-\eta_i$.
This arrangement allows for simple indexing of the memristances, and is the mechanism behind a Content-Addressable-Memory (CAM) which is used in crossbar arrays.
The idea of this construction dates back to Steinbuch's "Die Lernmatrix" (the learning matrix)~\citep{Steinbuch1961,Steinbuch1964}.

Crossbar arrays can be used for matrix-vector multiplication using the voltages $\bset{\xi_j}$ as inputs and the voltages $\bset{\eta_i}$ as outputs.
For a resistance independent of the input voltage or current, the relation between them is given by $\vec{\eta} = A \vec{\xi}$, where the elements of the matrix $A$ are:

\begin{equation}
    A_{ij} = \frac{ M_{ij}^{-1} }{ \tilde{R}_i^{-1} + \sum_{s=1} M_{is}^{-1} }.
\end{equation}

\noindent $\tilde{R}_i$ are the resistances on the output b-lines $\vec{\eta}$, and $M_{ij}$ is the memristance of the memristor between the $i$-th b-line and $j$-th e-line.
An algorithm for setting the (mem)resistances given a matrix can be found in~\citep{Xia2016}.
To apply this method with memristors, the voltages differences need to be small/short, to avoid changing their memristance, or all memristors need to be in one of their limiting states $\Ron$ or $\Roff$.

The component density of crossbar arrays can be increased by stacking various layers of crossbar arrays on top of each other~\citep{Strukov2009,Li2017}.
The stacking of $L$ crossbar layers scales the density of components by a factor $L$, i.e. a theoretical scaling of $\frac{L}{4\ell^2}$.
In multilayered arrays, memristors are controlled using the corresponding horizontal and vertical lines of each layer.

Memristive crossbar arrays can be used to encode the synaptic weights of feed-forward or recurrent artificial neural networks (ANNs)~\citep{Li2018}.
In ANNs the input to neurons in a given layer (post-synaptic) is computed as the multiplication of the outputs of neurons in the previous layer (pre-synaptic) and the matrix of synaptic weights connecting the two layers (Fig.~\ref{fig:annweights}).
Then, the multiplication is carried over using the multiplication algorithm previously described.
The output of pre-synaptic neurons is encoded in the voltages of e-lines of the crossbar array, and the input to post-synaptic neurons is decoded from the currents on the b-lines.
Applications go beyond this direct implementation of the multiplication algorithm.
For example in~\citep{Li2018} the synaptic weights are encoded as the difference of the conductance between two memristors.
Similar ideas have been exploited to design other computational models based on
stateful logic~\citep{Itoh2014} and differential pair synapses (called ``kT-RAM")~\citep{ktram}.

The potential benefits of utilizing memristor based ANNs are speed and energy efficiency.
The computation and storage use the same location in the network, and analog inputs are directly fed to the neurons.
This architecture minimizes the reading-writing costs incurred by the conventional von Neumann architecture, and potentially the energy losses of analog-to-digital conversion.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth,trim=0cm 15cm 0cm 0cm,clip]{figures/ANNweights.jpg}
    \caption{Memristors used for synaptic weights in ANNs. Reproduced with permission from Fig. 2 of~\citep{Li2018}}
    \label{fig:annweights}
\end{figure}

\subsection{Synaptic plasticity}
\label{sec:synapplast}
Synaptic plasticity can be broadly defined as the modification of the synaptic conductance as a function of the activity of the neurons connected to it.
This definition rules out autonomous plasticity, which is the slow synaptic conductance decay of inactive synapses (volatility).
Autonomous plasticity is fundamental for data processing applications and will be considered in section~\ref{sec:memdataproc}.

Non-autonomous synaptic plasticity can be classified in several types, e.g. spike-rate-dependent, spike-timing-dependent, short-term, long-term, etc.
In the study of synaptic plasticity of the human cortex, Hebbian or Anti-Hebbian (related to the simultaneous firing of two neurons)~\citep{Hebb1949,Kohonen1989,Koch9725} are often the underpinning learning mechanisms.
Our aim here is not to describe the types of plasticity, their biological origin, or how difficult it is to isolate each class in biological and experimental systems.
Our intention is but to illustrate how memristors, used as memories, can be used to implement the change in weights based on pre- and/or post-synaptic activity.

For an overview of the different types of synaptic plasticity and references to further reading, we refer the reader to the book chapter by~\citeauthor{LaBarbera2017}~\citep{LaBarbera2017}.

Synaptic plasticity is modeled by choosing the plasticity inducing variables $\bm{x} \in X$ (e.g. relative arrival times of spikes, relative neural activity, etc.) and a mapping from these to the change of the synaptic weight

\begin{equation}
  f_X: X \rightarrow W_{\Delta} \subset \mathbb{R}.
\end{equation}

\noindent This mapping is based on biological models, or simplified adaptation mechanisms.
For example in spike-timing-dependent Plasticity (STDP), the inducing variable is the relative timing of two or more activity spikes in the connected neurons.
Plasticity is then defined with a mapping  $f_{\text{STDP}}: T^{n-1} \rightarrow W_{\Delta}$, taking the relative timing of $n$ spikes $\in T^{n-1}$ (typically 2 or 3) to a weight change $ \in W_{\Delta}$ (usually represented as relative change).
In spike-rate-dependent Plasticity, we replace the timing domain with a relative rate domain.

In order to implement plasticity in memristors as synaptic weights, we need a writing voltage that represents the synaptic change.
That is, we need a further mapping

\begin{equation}
  f_V: W_{\Delta} \rightarrow V,
\end{equation}

\noindent where $V$ is the set of valid writing voltages.
The composed mapping $ f_V \circ f_X: X \rightarrow V$ gives the final implementation of synaptic plasticity.
The mapping $f_V$ depends on all the characteristics of the technology used, e.g. the physical mechanisms of memristance (see Appendix~\ref{sec:appphys}), the neural network architecture (e.g. crossbar array), the controlling electronics, etc.
Obtaining the function $f_V$ in the mapping above is the main challenge in synaptic plasticity applications, and thus requires considerable effort.
A survey of complete and partial implementations of synaptic plasticity in nanoscale devices is summarized in~\citep[][sec. 4.1]{LaBarbera2017}.
For example~\citep{Ielmini2018} uses STDP to implement unsupervised learning with resistive synapses.

STDP receives a lot of attention in the neuromorphic field as exemplified by the latter reference and the review of~\citeauthor{Serrano2013}, on which we base the following sentences.
The reader interested in hardware implementations of STDP should consult that resource.
STDP is among the most developed memristors implementation of in-silico plasticity, it can be implemented in very large and very dense arrays of memristors without global synchronization, and learning occurs online in a single integrated phase (as opposed to offline learning).
The impact of the dynamical model of the memristor has been studied in the implementation of STDP, and the learning rules can be adapted to the different behaviors.
As in most application of memristors as non-volatile storage of (synaptic) weights, it suffers from the intrinsic variability of the units, which more general neuromorphic circuits are able to exploit~\citep{Payvand2018}.
