The earliest known occurrence of a ``memristive" behavior in circuit components is in the study, by Sir Humprey Davy's, of arc (carbon) lamps~\citep{Davy}, dated to the late 19th century.
Another example, which was key to the discovery of the radio, is the coherer~\citep{Falcon2004,Falcon2005}.
The coherer was invented by Branly~\citep{Branly} after the studies of Calzecchi-Onesti, and inspired Marconi's radio receiver~\citep{Marconi}.
Branly's coherer serves as perfect homemade memristor~\citep{Bequin2010}, as it simply requires either a (fine) metallic filling or some metallic beads, and it falls within the Physics discipline of electrical properties of granular media.
Coherers are very sensitive to magnetic fields, and thus can act as a radio receiver and as we describe in the Appendix~\ref{sec:appphys}. They do posses a typical hysteric behavior which is associated with memristive components.
At the simplest level of abstraction, a memristor is a very peculiar type of nonlinear resistance with an hysteresis (e.g. they possess an internal memory).
Currently, the discussion is focused not only on the application of this technology to novel computation and memory storage, but also on the fundamental role of memristors in circuit theory~\citep{Abraham2018a}.
While this discussion is important, we first discuss why memristive behavior is not uncommon in analog computation.
We thus use the analog of hydraulic computers.

Hydraulic computers were built in Russia during the 1930s before valves and semiconductor were invented.
The hydraulic computers designed by the Russian scientist Vladimir Lukyanov were used to solve differential equations; %(Fig.~\ref{fig:hydrcomp}); 
other models like MONIAC~\citep{Strogatz2009}, would be later employed by the Bank of England to perform economic forecasts.

%\begin{figure}[htpb]
%  \centering
%  \includegraphics[scale=0.4]{figures/RussianComputer.jpg}
%  \caption{
%  Lukyanov's hydraulic computer or water integrator.
%  Picture from Moscow Polytechnical Museum.
%  }
%  \label{fig:hydrcomp}
%\end{figure}

The idea behind these computers was to use hydraulics to solve differential equations.
As Kirchhoff laws can be stated for any conservative field, we have that the pressure drop in a loop is equal to the actions of pumps in that loop.
The conservation of mass is equivalent to conservation of charge, and to Kirchhoff's second law: in the steady state, the mass of water entering a node must be equal to the mass flowing out.
Ohm's law is equivalent to Poiseuille's law in the pipes: a porous material in the pipes, or a constriction, is equivalent to a resistance.
A material that expands when wet, like a sponge, will increase the resistance to the flow as it absorbs the water.
This is equivalent to a higher resistance which depends on the amount of water that flowed thought the system, i.e. memristor-like.
The sponge, however, does not have a polarity, while memristor do depending on the physical mechanism which induces the switching.
Other memristor-like systems can be built with other mechanical analogs~\citep{Vongehr2015,Vongehr2015b}, plants and potato tubes~\citep{Volkov} or slime moulds~\citep{Gale2015,Gale2015a} just to name a few.

The modern history of memristors is tied to the work of Leon Chua. The first time the word memristor (as an abbreviation for memory resistor) appeared was in the now celebrated Chua 1971 article~\citep{Chua71}.
During the 1960's, Leon Chua worked extensively on the mathematical foundations of nonlinear circuits.
When he moved to Berkeley, where he currently is a Professor, he had already won several awards such as the IEEE Kirchhoff Award.
The definition of memristor was made clearer in a second paper with his student at the time, Sung Mo Kang~\citep{Chua1976}.
The second work was an important generalization of the notion of memristor and is the one we used in the present paper.
\citeauthor{Chua1976} introduced the notion of 'memristive device': a resistance which depends on a state variable (or variables), which is sufficient to describe the physical state (resistance) of the device at any time.
The component defined by~\citeauthor{Chua71}, and then by~\citeauthor{Chua1976}, is a resistance whose value depends on some internal parameter, which in turn has to evolve dynamically according either to current or voltage.
Implicitly, one needs to also define the relationship between the resistance and the state variable, which characterizes the device resistance.
In the analogy with hydraulic computers, the state variable represents by the density of holes in the sponge as a function of time, while the resistance is the amount of traversable area.
The 1976 and the 1971 papers were mostly mathematical and formal, without any connection to the physical properties of a real device.
The 1976 paper also introduced the fact that if one controls the device with a sinusoidal voltage, then one should observe Lissajous figures in the current-voltage (I-V) diagram of the device.
It also established that any electronic component that displayed a pinched hysteresis in the I-V diagram has to be a memristor.
The eager reader will find more details on the devices in the rest of the paper.

For the 30 years after the work by~\citeauthor{Chua1976}, the field of memristors was basically non-existent.
During the mid 2000's, Strukov, Williams and collaborators, working at Hewlett-Packard Labs were studying oxide materials and resistive switching.
The physics of resistive switching was known since the early '70s, with the introduction of Phase-Change Materials.
The physical origin of resistive switching was well studied, albeit not fully understood~\citep{Waser2007,Waser2007b,Aono2010}.
The idea that memristors could be experimentally realized became popular with the article of~\citeauthor{Strukov2008} at HP Labs.
