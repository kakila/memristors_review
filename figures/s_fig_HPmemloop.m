% Copyright (C) 2018 Juan Pablo Carbajal
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program. If not, see <http://www.gnu.org/licenses/>.
%
% Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
% Created: 2018-08-25

%% I-V curve of the HP-memristor for different frequencies
%
V    = @(t, f) sin (2 * pi * f * t);

Ron  = 1e3;
Roff = 6e3;
R    = @(w) Ron * (1 - w) + Roff * w;

I    = @(t, w, f) V (t, f) ./ R (w);

beta = 3e-4;
alph = 0.0;
dwdt = @(t, w, f) alph * w - I (t, w, f) / beta;

function [pos,ter,dir] = Wevent (t, w)
  pos = w - [0; 1];
  ter = [1; 1];
  dir = [-1 1];
endfunction

freq = [0.95 1.2 3 10];
nf   = length (freq);
W    = cell (1, nf);
t    = cell (1, nf);
for i=1:nf
  T = 1 / freq(i);
  dt = T / 200;
  opt = odeset ('MaxStep', dt, 'InitialStep', dt, 'Events', @Wevent);
  [t{i}, W{i}] = ode45 (@(x,y)dwdt (x,y,freq(i)), [0 T], 0.5, opt);
end

figure (1), clf
  hold on
  for i=1:nf
    v     = V (t{i}, freq(i));
    vpp   = [min(v) max(v)];
    c     = I(t{i}, W{i}, freq(i)) * 1e3;
    pp    = polyfit (v, c, 1);
    h(i)  = plot (v, c, '-', 'linewidth', 5);
    plot (vpp, polyval(pp, vpp), '--', 'color', get (h(i), 'color'), 'linewidth', 5);

    if abs(fix(freq(i)) - freq(i)) < eps(freq(i))
      lbl{i} = sprintf ("%d Hz", freq(i));
    else
      lbl{i} = sprintf ("%.1f Hz", freq(i));
    end%if
  end%for
  hold off
  axis tight
  grid on
  set (gca, 'gridlinestyle', ':', 'fontsize', 38);
  xlabel ('Voltage [V]')
  ylabel ('Current [mA]')

  outerpos  = get (gca, 'OuterPosition');
  ti        = get (gca, 'TightInset');
  left      = outerpos(1) + ti(1);
  bottom    = outerpos(2) + ti(2);
  ax_width  = outerpos(3) - ti(1) - ti(3);
  ax_height = outerpos(4) - ti(2) - ti(4);
  set (gca, 'Position', [left bottom ax_width*0.99 ax_height]);

  hleg = legend (h, lbl, 'Location', 'NorthWest');
  set (hleg, 'color', 'w', 'fontsize', 38);

imwrite (frame2im (getframe (gcf)), 'HPmemloop.png')
