In the main text we have mentioned that Branly's coherer can be interpreted as a granular material induced memristor. Before we discuss some more modern memristors, we believe it makes sense to give a sense why granularity is important for nonlinear resistive behavior.
Branly's coherer serves as perfect homemade memristor, as it simply requires either a (fine) metallic filling or some metallic beads, and it falls within the Physics discipline of electrical properties of granular media.
The qualitative and quantitative behavior of the case of the metallic beads contained in (and constrained to) an insulating medium of PVC is presented below~\citep{Falcon2005,Bequin2010}.
The metallic beads are assumed to be in mutual contact, and a force $F$ applied at the two extremities enforces it.
We assume that there are two temperatures in the system one at the microcontact between the beads at an equilibrium temperature $T$~\citep{Falcon2005} and a room temperature $T_0$ which is the one of the beads.
Without going into the details, it is possible to show a nonlinear behavior in the resistivity of $N$ beads via the study of the contact between the beads.
The metallic beads contacts can be though of as Metal-Oxide-Oxide-Metal contact, reminiscent of the memristor we will discuss below.
Kohlrausch’s equation establishes the voltage drop at the contact.
Given the current $I$ flowing in the beads, one has that

\begin{equation}
    V=N L \int_{T_0}^{T_{c}} \lambda(T) \rho_{el}(T) dT,
\end{equation}

\noindent where $\lambda(T)$ is the thermal conductivity of the material and $\rho_{el}(T)$ the density of electrons, while $T_0$ is the beads room temperature and $T_c$ the maximum temperature at the contact when the current is flowing.
If $R_0$ is the resistance when the contact is cold, clearly we can rewrite (using Ohm's law) the equation above as the following effective equation:

\begin{equation}
    I R_0=V(T).
\end{equation}

\noindent where $R_0$ depends on the geometry of the contact.
It can be shown however that the maximum temperature $T_c$ depends on the voltage as $T_m^2=T_0^2+\frac{U^2}{4 L}$ where $L$ is the Lorentz constant, by noticing that via the Wiedemann-Franz that $\rho_{el} \lambda= L T$.
Also Mathiesen's rule for the electron mobility shows that the electron mobility is linear in $T$, with a proportionality constant that is material dependent.
Putting all these facts together, it is not hard to see the hysteretic behavior of the system.
This effective model reproduces well the controlled experiments.
Since the voltage drop is zero when the current is zero, this also implies a pinched hysteresis, or resistive behavior.
These ``mechanical" nonlinear resistors are prototypes for the more complicated case shown below~\citep[see][for more details]{Falcon2005}.
A posteriori, many phenomena, both quantum and classical, have been reinterpreted as ``memristors" (for instance, Josephson junctions~\citep{supercm}).
Below We provide a non-exhaustive list of mechanisms that have been discussed in the literature. For more detailed information, we suggest the reviews on the subject of resistive switching given in~\citep{DiVentra2011,Kuzum2013,Yang2013,Mikhaylov2016}.


\subsection{Phase change materials}
Phase change materials (PCM) are glassy materials: this implies that usually these materials can have different phases in which the material can be either ordered (crystalline-like) or disordered (amorphous, as in a liquid).
In these two phases we associate two different resistances.
If the structural change can be associated to the values of an applied voltage, then when the transition occurs one has a rather quick transition from one resistive state to another~\citep{Ovshinsky1968,Neale70,Buckley1975} due to an electrical instability.
These materials are considered memristors by some, but not by all researchers, and were discovered as early as the late '60s~\citep{Ielmini2011} in amorphous chalcogenides.
This is the most mature of the emerging memory technologies.
Since we have used various analogies before, the reader in need of a visual way to understand these type of materials might find some ideas on the shelves of a pharmacy.
Phase-change materials are being used for instant freeze packages normally used in case of injuries, and are also called ``gel packs".
In order to initiate the cooling, users typically need either to mix two materials, or the quickly apply a certain force on the package.
The material will use the ``kick" to initiate a phase transition, absorb the heat and thus lower the temperature of the package.
PCM memory devices work in a similar manner, but on a much lower scale ($\sim 20$ nm), and the ``kick" is provided by the electric field. These materials were not commercial for years due to the rapid advancement of silicon-based technology.
The typical I-V diagram is shown in Fig.~\ref{fig:PCM}.
One generically has two type of materials squeezed between two electrodes: on one side one has an insulating material with a small conducting channel, directly connected to the phase change material.
As the channel heats up, the phase change material locally changes phase starting from point of contact at the conduction channel, until it reaches the other electrode.

In some chalcogenides-based memristors (called Self-Directed channel memristors, or SDC), ions are constrained to follow certain channels, and their operation is similar in many ways to both PCM-components and atomic switches~\citep{campbell}.

\begin{figure}
    \centering
    \includegraphics[scale=2]{PCM.png}
    \caption{Current-Voltage diagram typical of a PCM memory storage. The sharp transition at low currents and at a typical ``Critical voltage" is clearly visible.}
    \label{fig:PCM}
\end{figure}

\subsection{Oxide based materials}
The oxide and anionic materials based on transition metal work instead differently.
In order to understand why a memristor might be different from a normal conductor it is useful to understand what happens when two materials with different properties are ``merged" together: those of charge donor (excess of electrons) and charge receiver (excess of electron ``holes"), which are also called doped and undoped materials.
In oxide materials the carrier of the charge is typically the oxygen.
It should be mentioned that whilst various mechanisms have been suggested, very likely all of these coexist in a typical oxide material, including filament formation~\citep{Hoskins2017}.
In general, whether the resistive switching is thermally or electrically driven, the typical understanding is that a chemical transition occurs in the material, and that the hysteresis is due to vacancy movements in the materials.
The transition is either directly driven by the direct application of the electric field, or as a byproduct of the heating of the material due to the current.
In semiconductors, the key quantity of interest is the energy gap $E_g$ between the valence and the conduction channels.
If the gap is of the order of $E_p \approx k T$, then thermal effects which might let a charge carrier jump into the conduction channel become important.
If the gap is too large, electric effects are the dominant ones.
One example is provided by the bipolar and non-volatile switching, described by  Fig.~\ref{fig:bipnonv}.

\begin{figure}
    \centering
    \includegraphics[scale=2]{BipNonV.png}
    \caption{Bipolar and non-volatile switching pinched hysteresis.}
    \label{fig:bipnonv}
\end{figure}

The shape of I-V curve generically shows what type of mechanism is underlying the switching.
As the field effects become more prominent (for instance, the effect of Schottky barriers at the junctions), the I-V diagram becomes nonlinear.
There can be other non-volatile and non-volatile switching in which we are not discussing here, and due to thermal excitation only.
A simplified model of vacancy-charge movement in the dielectric has been proposed in~\citep{Chernov2016}.
When nonlinearities are present in non-volatile materials, it means that the switching is dominated
by the electrical switching.
In many ways, some of the physical phenomena happening in memristors can be intuitively understood in terms of the simplest semiconductor: the diode, represented in Fig.~\ref{fig:pnj} (a).

\begin{figure}
    \centering
    \includegraphics[scale=1.5]{pnj.png}
    \caption{The pn-junction. (a) The pink region is the charge exchange region for the doped and undoped region. (b) A stylized response in voltage of a diode. In many ways, the shape resembles the nonlinearity which occur in nonlinear memristors in which field effects become dominant.}
    \label{fig:pnj}
\end{figure}

The diode can be thought of as a dramatically nonlinear resistance, made by merging two material, a doped (filled with defects) and undoped one, with a thin interstitial when charges exchange.
We can characterize the diode by the two voltages $V_{break}<0<V_{crit}$.
For voltages above $V_{crit}$, the diode will have a very low resistance, and for voltages lower  than $V_{break}$, the diode will breakdown.
In many ways, the shape of Fig.~\ref{fig:pnj} (b) is what is usually seen in memristive oxide components, with the difference that the memristor will exhibit a hysteresis.
At the interfaces (pink zone in Fig.~\ref{fig:pnj} (a)), the charge carriers will have to overcome a barrier (Schottky barrier) (characterized by $V_{crit}$) to continue their flow into the material.
When this barrier is overcome, the flow is almost free.
On the other hand, when the flow is inverted, the undoped material will act as a very high barrier to the flow into the dielectric region.
However, the charge carriers can still penetrate the material and damage it in an irreversible manner.
These are usually called  Zener cascades, and are often observed in memristors.
Albeit cartoonish, this picture can help understand some of the nonlinear phenomena happening in memristors, and not captured by the linear resistance model we discussed above.
As a final comment, endurance in oxide materials is one of the key problems in the technological competitiveness of memristors~\citep{IelminiEndurance2015}.
When the number of cycles becomes comparable with the lifetime of the component, also some exotic phenomena as multiple pinchpoints (tri- or four-lobes hysteresis loops) in the V-I diagram can occur.
These can be modeled via the introduction of fractal derivatives~\citep{8351761}.

\subsection{Atomic switches}
The third mechanism described in this paper (albeit not the last!), and maybe the most visually appealing, is the filament growth memristor.
The materials, often called \textit{atomic switch networks} in the literature, work in a slightly different manner than the previously described memristors~\citep{Stieg2014,Sillin2013,Avizienis,Stieg12,AtomicSwitch2}.
The idea behind these components is that the two electrodes work, when the electrical field is applied, as an electrode and a cathode.
The applied electric field induces an electrochemical reaction which triggers the growth of filaments.
From a highly resistive state, these filaments reduce the resistivity by introducing new channels for the charge carriers to flow.
The closest physical growth model which can describe the growth of the filaments is provided by Diffusion-Limited-Aggregation (as in Fig.~\ref{fig:dla}).
Each colored filament represents one possible channel. The typical charge carriers are either silver ions or some silver compounds.
There are not many experiments focused instead on the collective behavior of memristive networks. It is worth mentioning however what are the observed features for $Ag^+$, or Atomic Switch Networks, whose collective dynamics is interesting for the emergence of seemingly critical states~\citep{Scharnhorst2018}.
In fact, whilst the dynamic of a single filament is simpler to describe, the system exhibits collective power law power spectrum with an exponent close to 2.
Albeit this exponent can be explained via the superposition of wide range of relaxation timescales for each memristor~\citep{Caravelli2017}, the critical behavior is the accepted one because of their intrinsic nonlinearity~\citep{Avizienis}.
\begin{figure}[H]
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=0.6\textwidth]{branch.jpeg}
    \caption{Dendritic growth in silver ion at the micro meter scale.
Reproduced with permission} from~\citep{Wen2006}.
    \label{fig:dendr}
\end{subfigure}
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=0.5\textwidth]{DLA.png}
    \caption{Diffusion-Limited-Aggregation simulated using NetLogo.}
    \label{fig:dla}
\end{subfigure}
\caption{Comparison between dendritic growth in silver ion materials and Diffusion-Limited-Aggregation simulated using NetLogo.}
\end{figure}
In order to see the variety of memristive behavior, ere we consider two models suggested in the literature which are different than the simple $TiO_2$ linear model memristor.

The first, suggested in ~\citep{Pickett2009} as a phenomenological switching model between two off and on states in $TiO_2$, is of the form:
\begin{align}
R(w) &= \Roff (1-w)+\Ron w,\\
\tder{w}{t}&= \begin{cases}
f_{\text{off}} \sinh(\frac{i}{i_{\text{off}}}) e^{-e^{\frac{w-a_{\text{off}}}{w_c}-\frac{|i|}{b}}-\frac{w}{w_c}} & i>0, \\
f_{\text{on}} \sinh(\frac{i}{i_{\text{on}}}) e^{-e^{-\frac{w-a_{\text{on}}}{w_c}-\frac{|i|}{b}}-\frac{w}{w_c}} & i<0,
\end{cases}
\end{align}
\noindent where the parameters $f_\text{on/off}$, $i_\text{on/off}$ and $a_\text{on/off}$ are state dependent, while $w_c$ and $b$ are not.
Also, the model above shows that the energy depends exponentially on the current.

Another metal oxide of interest is $WO_x$, studied in~\citep{Chang2011}.
The model equations for a single component are given by:

\begin{align}
    I&=\alpha (1-w) \left(1-e^{\beta V} \right) + w\gamma \sinh(\delta V), \\
    \tder{w}{t}&= \lambda \left(e^{\eta_1 V}-e^{-\eta_2 V} \right).
\end{align}

The model above, it is interesting to note, is controlled in voltage rather than current, and the parameters $\alpha,\beta,\gamma,\delta$ and $\eta_{1}$ and $\eta_{2}$ are positive.
For $V=0$, $I=0$, which implies a pinched hysteresis (however, the hysteresis is different from $TiO_2$ devices).
The parameter $w$ is physically interpreted as the portion of the device in which oxygen charges tunnel through the device.
For $w=1$ one has tunneling dominated device, while at $w=0$ one has a Schottky-dominated conduction.

One question which might be relevant to mention at this point is: what is the advantage of using memristors rather than other memory devices?
The perspectives of the present paper is that there are two different reasons why these devices can turn useful~\citep{itrs11}.
The first advantages is the density compared to standard memory.
For instance, compared to DRAM and SRAM, memristors (or PCM) retain memory for years rather than less than seconds.
Compared to SRAM however, whose read-write time is less than a nanosecond, memristors with current technology are one order of magnitude slower.
Also, in terms of read-write cycles, the technology of memristors and PCM is between $3-5$ order of magnitude less, but still much more durable than HDD.
The picture is that memristors and PCM are not uniquely better than the current standard in computing.
\subsection{Spin torque}
Spin-torque memory materials ~\cite{stt} have an advantage in terms of durability~\citeauthor{Grollier2016} over other materials such as transition metal oxides.
These are often considered as second-order memristive devices but a simplified model of spin-torque induced resistance is provided in~\citep{XiaobinWang2009}.
The starting point is the Landau-Ginzburg-Gilbert (LGG) equation with a spin-torque interaction~\citep{Sun2000}. We consider two magnetic layers perpendicular to the flow of the current, and in which one is fully polarized: its magnetic orientation is fixed in a direction perpendicular to the current, while the second layer is free. Via the LGG equation with rotational symmetry, the dynamics of the angle between the pinned and free layer is given by
\begin{equation}
    \tder{\theta(t)}{t}=\alpha \gamma H_k \sin\left(\theta(t)\right) \left(p-\cos \left(\theta(t)\right)\right),
\end{equation}
where $\gamma$ is called gyromagnetic ratio, $\alpha$ is the damping parameter, $H_k$ is the perpendicular anysotropy in the free layer and $p=f \hbar I$ is a current dependent which represents the effect of the current-polarization interaction. We have emphasized the presence of $\hbar$ to imply that this is a purely quantum correction.

In order to see how this device is a memristor, we see that in the simpler case with full rotational symmetry one has an induced magneto-resistance $R(\theta)$, which depends on the angle $\theta$ in the case of full rotational symmetry as:
\begin{equation}
    R\left(\theta(t)\right)=\frac{1}{a +b \cos\left(\theta(t)\right)},
\end{equation}
where $a$ and $b$ are constants which depend on the resistance in the free layer and on the ration between the highest and lowest achievable resistances. We thus see that in the case \textcolor{red}{with}\textcolor{green}{of} full rotational symmetry, spin-torque materials are first order memristors.

\subsection{Mott memristors}
In this section we discuss resistive switching due to the Mott between insulating and conducting phase in metals~\citep{mottmemr,HopMemr2}.
In the Mott transition, the important quantity of interest is the electron density, which acts as a control parameter for the transition.
This implies that, in general, the transition can be induced via pressure change or doping.
The transition is usually studied in the context of weakly correlated electron liquids, in which the Coulomb interactions are screened and thus can be considered weak.
In general, there are various known mechanisms that enable an insulator transition.
At the single electron (charge carriers in general), there can be band insulators, which we have discussed before in the context of oxide based materials; Peierls insulators~\citep{peierlst}, which are due to lattice deformations that distort the band gaps; or Anderson insulators~\citep{andersont}, an effect due to the interaction of a particle with the disorder in the materials in which, at the quantum level, the charge carrier becomes localized.

The Mott transition occurs due an electron-electron interaction, and leads to the formation of a gap between the ground state of the system and the excited state: this implies that in order to kick a charge into the conduction band, the system requires to overcome a barrier.
The transition occurs because of the repulsion between the electrons, which impedes the free flow of these into the material.
Mott memristors take advantage of the fact that certain materials (such as niobium dioxide~\citep{niobium}) have a current induced Mott transition.
This is due to the fact that as the current is enough to locally heat the material above a certain threshold value, the material undergoes phase transition.
