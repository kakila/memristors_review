%  LaTeX support: latex@mdpi.com
%  In case you need support, please attach all files that are necessary for compiling as well as the log file, and specify the details of your LaTeX setup (which operating system and LaTeX version / tools you are using).

% You need to save the "mdpi.cls" and "mdpi.bst" files into the same folder as this template file.

%=================================================================
\documentclass[technologies,perspective,accept,moreauthors,pdftex,10pt,a4paper,dvipsnames]{mdpi}

% If you would like to post an early version of this manuscript as a preprint, you may use preprint as the journal and change 'submit' to 'accept'. The document class line would be, e.g. \documentclass[preprints,article,accept,moreauthors,pdftex,10pt,a4paper]{mdpi}. This is especially recommended for submission to arXiv, where line numbers should be removed before posting. For preprints.org, the editorial staff will make this change immediately prior to posting.

%=================================================================
\firstpage{1}
\makeatletter
\setcounter{page}{\@firstpage}
\makeatother
\articlenumber{1}
\pubvolume{xx}
\pubyear{2018}
\copyrightyear{2018}
%\externaleditor{Academic Editor: name}
\history{Received: date; Accepted: date; Published: date}
\issuenum{1}
%\updates{yes} % If there is an update available, un-comment this line

%------------------------------------------------------------------
% The following line should be uncommented if the LaTeX file is uploaded to arXiv.org
%\pdfoutput=1

%=================================================================
% Add packages and commands here. The following packages are loaded in our class file: fontenc, calc, indentfirst, fancyhdr, graphicx, lastpage, ifthen, lineno, float, amsmath, setspace, enumitem, mathpazo, booktabs, titlesec, etoolbox, amsthm, hyphenat, natbib, hyperref, footmisc, geometry, caption, url, mdframed, tabto, soul, multirow, microtype, tikz, siunitx
\usepackage{subcaption} % for subfigures
\usepackage{natbib}
\usetikzlibrary{shapes.arrows, shapes, shadows}

\newcommand{\FC}[1]{\textcolor{red}{FC: {#1}}}
\newcommand{\JPC}[1]{\textcolor{olive}{JPC: {#1}}}

\newcommand{\stimes}{\!\times\!}                     % Small spaces x, used for matrix dimensions
\newcommand{\bm}[1]{\boldsymbol{#1}}                 % Bold math, pkg bm causes troubles
\newcommand{\bset}[1]{\big\lbrace {#1} \big\rbrace}  % Set by extension using index {g_i}
\newcommand{\ud}{\mathrm{d}}
\newcommand{\pder}[2]{\frac{\partial{#1}}{\partial{#2}}}
\newcommand{\dpder}[2]{\frac{\partial^2{#1}}{\partial{#2^2}}}
\newcommand{\sderp}[3]{\frac{\partial^2{#1}}{\partial{#2}\partial{#3}}}
\newcommand{\tder}[2]{\frac{\ud{#1}}{\ud{#2}}}
\newcommand{\integ}[2]{\int {#1}\ud {#2}}
\newcommand{\braket}[1]{\langle{#1}\rangle}

\newcommand{\Ron}{R_{\text{on}}}                     % Low resistance state
\newcommand{\Roff}{R_{\text{off}}}                   % High resistance state
\newcommand{\resratio}{r_{\text{on}}}                % Ratio of Ron to resistance span
\newcommand{\resspan}{\Delta R}                      % resistance span

\DeclareMathOperator{\diag}{diag}
\DeclareMathOperator{\trace}{trace}

\graphicspath{{figures/}}

%=================================================================
%% Please use the following mathematics environments: Theorem, Lemma, Corollary, Proposition, Characterization, Property, Problem, Example, ExamplesandDefinitions, Hypothesis, Remark, Definition
%% For proofs, please use the proof environment (the amsthm package is loaded by the MDPI class).

%=================================================================
% Full title of the paper (Capitalized)
\Title{Memristors for the curious outsiders}

% Author Orchid ID: enter ID or remove command
\newcommand{\orcidauthorA}{0000-0001-7964-3030} % Add \orcidA{} behind the author's name
\newcommand{\orcidauthorB}{0000-0002-6556-1760} % Add \orcidB{} behind the author's name

% Authors, for the paper (add full first names)
\Author{Francesco Caravelli$^{1}$\orcidA{} and Juan Pablo Carbajal $^{2,3}$\orcidB{}}

% Authors, for metadata in PDF
\AuthorNames{Francesco Caravelli, Juan Pablo Carbajal}

% Affiliations / Addresses (Add [1] after \address if there is only one affiliation.)
\address{%
$^{1}$ \quad Theoretical Division and Center for Nonlinear Studies, Los Alamos National Laboratory, Los Alamos, New Mexico 87545, USA; caravelli@lanl.gov\\
$^{2}$ \quad HSR University of Applied Sciences Rapperswil, Institute for Energy Technology, 8640 Rapperswil, Switzerland; juan.pablo.carbajal@hsr.ch\\
$^{3}$ \quad Swiss Federal Institute of Aquatic Science and Technology, Eawag, Überlandstrasse 133, 8600 Dübendorf, Switzerland; juanpablo.carbajal@eawag.ch}

% Contact information of the corresponding author
%\corres{Correspondence: e-mail@e-mail.com; Tel.: +x-xxx-xxx-xxxx}

% Current address and/or shared authorship
%\firstnote{Current address: Affiliation 3}
%\firstnote{These authors contributed equally to this work.}
% The commands \thirdnote{} till \eighthnote{} are available for further notes

% Simple summary
%\simplesumm{}

% Abstract (Do not insert blank lines, i.e. \\)
\abstract{We present both an overview and a perspective of recent experimental advances and proposed new approaches to performing computation using memristors.
A memristor is a 2-terminal passive component with a dynamic resistance depending on an internal parameter.
We provide an brief historical introduction, as well as an overview over the physical mechanism that lead to memristive behavior.
This review is meant to guide nonpractitioners in the field of memristive circuits and their connection to machine learning and neural computation.}

% Keywords
\keyword{memristors; neuromorphic computing; analog computation; machine learning}

% The fields PACS, MSC, and JEL may be left empty or commented out if not applicable
%\PACS{J0101}
%\MSC{}
%\JEL{}

\begin{document}
\tableofcontents

\newpage

\section{Introduction}
\input{b_introduction.tex}

\section{Brief history of memristors}
\label{sec:history}
\input{c_memrhistory.tex}

\section{Mathematical models of memristors}
\label{sec:models}
\input{d_memrmodels.tex}

\section{Memristors for storage}
\label{sec:memstorage}
\input{e_memrstorage.tex}

\section{Memristors for data processing}
\label{sec:memdataproc}
\input{f_memr4dataproc.tex}

\section{Memristive galore!}
\label{sec:memgalore}
\input{g_memgalore.tex}

\section{Closing remarks}
\input{h_closing.tex}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{6pt}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ \\
\acknowledgments{We thank Fabio L. Traversa, F. Sheldon, Magdalena E. Dale, Giacomo Indiveri, Alex Nugent,  Miklos Csontos, Themis Prodromakis, Yogesh Joglekar, for their useful comments and observations, and Shihe Yang, Teuvo Kohonen, Themis Prodromakis, Yogesh Joglekar and Qiangfei Xei for the permission of using figures from their work for this review.
\textbf{FC} acknowledges the support of NNSA for the U.S. DoE at LANL under Contract No. DE-AC52-06NA25396.
\textbf{JPC} received support from the discretionary funding scheme of The Swiss Federal Institute of Aquatic Science and Technology project EmuMore.}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ \\
\authorcontributions{FC and JPC contributed equally to this work.}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\conflictsofinterest{The authors declare no conflict of interest.}
%% optional
\appendixtitles{no} %Leave argument "no" if all appendix headings stay EMPTY (then no dot is printed after "Appendix A"). If the appendix sections contain a heading then change the argument to "yes".
\appendixsections{multiple} %Leave argument "multiple" if there are multiple sections. Then a counter is printed ("Appendix A"). If there is only one appendix section then change the argument to "one" and no counter is printed ("Appendix").

\appendix
\section{Physical mechanisms for resistive change materials}
\label{sec:appphys}
\input{x_app_memphysics.tex}
\section{Sparse coding example}
\label{sec:scod}
\input{x_app_sparsecoding.tex}

%=====================================
% References, variant B: external bibliography
%=====================================
\externalbibliography{yes}
\bibliography{z_references}
\end{document}
